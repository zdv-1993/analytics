"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
# coding=utf-8
from django.conf import settings
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.staticfiles.views import serve
from django.views.generic import TemplateView
from apps import views
from apps.views import AddEvent, ListEvent, RealTimeEvent

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', TemplateView.as_view(template_name='index.html')),
    url(r'^list/$', ListEvent.as_view(), name='list'),
    url(r'^real/$', RealTimeEvent.as_view(), name='real'),
    url(r'^oauth2callback', 'apps.views.auth_return'),
    url(r'^accounts/login/', 'django.contrib.auth.views.login',
                        {'template_name': 'login.html'}, name='login'),
    url(r'^add/$', AddEvent.as_view(), name='add'),
    url(r'^success/$', TemplateView.as_view(template_name='events/success.html')),
    url(r'^welcome/$', TemplateView.as_view(template_name='welcome.html')),
    url(r'^static/(?P<path>.*)$', serve),
]

urlpatterns += staticfiles_urlpatterns()

