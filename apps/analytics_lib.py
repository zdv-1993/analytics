from googleapiclient.discovery import build
import httplib2
from oauth2client.contrib import xsrfutil
from oauth2client.client import flow_from_clientsecrets
from oauth2client.contrib.django_orm import Storage
from apps.models import CredentialsModel


class Singleton(type):
    def __init__(cls, name, bases, dict):
        super(Singleton, cls).__init__(name, bases, dict)
        cls.instance = None

    def __call__(cls,*args,**kw):
        if cls.instance is None:
            cls.instance = super(Singleton, cls).__call__(*args, **kw)
        return cls.instance


class SingletoneGoogleApi(object):
    __metaclass__ = Singleton

    def __init__(self, user):
        super(SingletoneGoogleApi, self).__init__()
        storage = Storage(CredentialsModel, 'id', user, 'credential')
        credential = storage.get()
        http = httplib2.Http()
        http = credential.authorize(http)
        self.service = build("analytics", "v3", http=http)

    def get_profiles(self):
        accounts = self.service.management().accounts().list().execute()

        if accounts.get('items'):
            account = accounts.get('items')[0].get('id')
            # Get a list of all the properties for the first account.
            return self.service.management().webproperties().list(
                accountId=account).execute()

    def get_metrics(self, profile_id):
        accounts = self.service.management().accounts().list().execute()

        if accounts.get('items'):
            account = accounts.get('items')[0].get('id')
            return self.service.data().realtime().get(
                ids='ga:' + profile_id,
                metrics='rt:activeUsers').execute()

    def insert_webProperty(self, web_site_url, web_site_name):
        accounts = self.service.management().accounts().list().execute()

        if accounts.get('items'):
            account = accounts.get('items')[0].get('id')
            body = {
                'websiteUrl': web_site_url,
                'name': web_site_name,
                # "permissions": {
                #     "effective": ['COLLABORATE', 'MANAGE_USERS', 'READ_AND_ANALYZE']
                # },
                # "account_id":account

            }
            return self.service.management().webproperties().insert(
                accountId=account,
                body=body
            ).execute()

    def get_first_profile_id(self):
      # Use the Analytics service object to get the first profile id.

      # Get a list of all Google Analytics accounts for this user
      accounts = self.service.management().accounts().list().execute()

      if accounts.get('items'):
        # Get the first Google Analytics account.
        account = accounts.get('items')[0].get('id')

        # Get a list of all the properties for the first account.
        properties = self.service.management().webproperties().list(
            accountId=account).execute()

        if properties.get('items'):
          # Get the first property id.
          property = properties.get('items')[0].get('id')

          # Get a list of all views (profiles) for the first property.
          profiles = self.service.management().profiles().list(
              accountId=account,
              webPropertyId=property).execute()

          if profiles.get('items'):
            # return the first view (profile) id.
            return profiles.get('items')[0].get('id'), profiles.get('items')[0].get('websiteUrl'),

      return None

    def get_profile_id(self, index):
      # Use the Analytics service object to get the first profile id.

      # Get a list of all Google Analytics accounts for this user
      accounts = self.service.management().accounts().list().execute()

      if accounts.get('items') and len(accounts.get('items'))>index:
        # Get the first Google Analytics account.
        account = accounts.get('items')[index].get('id')

        # Get a list of all the properties for the first account.
        properties = self.service.management().webproperties().list(
            accountId=account).execute()

        if properties.get('items'):
          # Get the first property id.
          property = properties.get('items')[index].get('id')

          # Get a list of all views (profiles) for the first property.
          profiles = self.service.management().profiles().list(
              accountId=account,
              webPropertyId=property).execute()

          if profiles.get('items'):
            # return the first view (profile) id.
            return profiles.get('items')[index].get('id')

      return None

    def get_results(self, profile_id):
      # Use the Analytics Service Object to query the Core Reporting API
      # for the number of sessions within the past seven days.
      return self.service.data().ga().get(
          ids='ga:' + profile_id,
          start_date='7daysAgo',
          end_date='today',
          metrics='ga:sessions').execute()

    def print_results(self, results):
      # Print data nicely for the user.
      if results:
        print 'View (Profile): %s' % results.get('profileInfo').get('profileName')
        print 'Total Sessions: %s' % results.get('rows')[0][0]

      else:
        print 'No results found'