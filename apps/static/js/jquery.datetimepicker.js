



<!DOCTYPE html>
<html lang="en" class=" is-copy-enabled is-u2f-enabled">
  <head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# object: http://ogp.me/ns/object# article: http://ogp.me/ns/article# profile: http://ogp.me/ns/profile#">
    <meta charset='utf-8'>

    <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/frameworks-473c88d8b41db1c58a1816fd5e5ab2cad86020e5698c68b7791505221ba933db.css" integrity="sha256-RzyI2LQdscWKGBb9XlqyythgIOVpjGi3eRUFIhupM9s=" media="all" rel="stylesheet" />
    <link crossorigin="anonymous" href="https://assets-cdn.github.com/assets/github-1eedcf6b67f697efe5e82b7cf2c02ea348abd63d45f4808f2690571941c507fa.css" integrity="sha256-Hu3Pa2f2l+/l6Ct88sAuo0ir1j1F9ICPJpBXGUHFB/o=" media="all" rel="stylesheet" />
    
    
    
    

    <link as="script" href="https://assets-cdn.github.com/assets/frameworks-635d1ea76a4969944a5443635e59661f12739d4b9e7a2749d185a22322072877.js" rel="preload" />
    
    <link as="script" href="https://assets-cdn.github.com/assets/github-1587f92a0de9322175ca288e3e883cd641f717202a2a1f473269ec5da4c65b92.js" rel="preload" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta name="viewport" content="width=1020">
    
    
    <title>datetimepicker/jquery.datetimepicker.min.js at master · xdan/datetimepicker</title>
    <link rel="search" type="application/opensearchdescription+xml" href="/opensearch.xml" title="GitHub">
    <link rel="fluid-icon" href="https://github.com/fluidicon.png" title="GitHub">
    <link rel="apple-touch-icon" href="/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon-180x180.png">
    <meta property="fb:app_id" content="1401488693436528">

      <meta content="https://avatars3.githubusercontent.com/u/794318?v=3&amp;s=400" name="twitter:image:src" /><meta content="@github" name="twitter:site" /><meta content="summary" name="twitter:card" /><meta content="xdan/datetimepicker" name="twitter:title" /><meta content="datetimepicker - jQuery Plugin Date and Time Picker" name="twitter:description" />
      <meta content="https://avatars3.githubusercontent.com/u/794318?v=3&amp;s=400" property="og:image" /><meta content="GitHub" property="og:site_name" /><meta content="object" property="og:type" /><meta content="xdan/datetimepicker" property="og:title" /><meta content="https://github.com/xdan/datetimepicker" property="og:url" /><meta content="datetimepicker - jQuery Plugin Date and Time Picker" property="og:description" />
      <meta name="browser-stats-url" content="https://api.github.com/_private/browser/stats">
    <meta name="browser-errors-url" content="https://api.github.com/_private/browser/errors">
    <link rel="assets" href="https://assets-cdn.github.com/">
    <link rel="web-socket" href="wss://live.github.com/_sockets/MTM3ODc3NDk6NmNkYzRhZDZjODI4OWM0ODk1Y2NlZDllMGRhZGU1ODM6N2ZjNDY1ZGQzMmI5OGM3OWMzZjUwNzVkM2RlYjQ1NWU2YTVmNTY5NmE0ZDE3ZmE3YWJiYjJkMTNhOWRjOGE1OA==--8572198cc1c7cb5ee891749a0b4c7ee3f100f350">
    <meta name="pjax-timeout" content="1000">
    <link rel="sudo-modal" href="/sessions/sudo_modal">

    <meta name="msapplication-TileImage" content="/windows-tile.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="selected-link" value="repo_source" data-pjax-transient>

    <meta name="google-site-verification" content="KT5gs8h0wvaagLKAVWq8bbeNwnZZK1r1XQysX3xurLU">
<meta name="google-site-verification" content="ZzhVyEFwb7w3e0-uOTltm8Jsck2F5StVihD0exw2fsA">
    <meta name="google-analytics" content="UA-3769691-2">

<meta content="collector.githubapp.com" name="octolytics-host" /><meta content="github" name="octolytics-app-id" /><meta content="B24E39E2:6757:737CB4A:5727089B" name="octolytics-dimension-request_id" /><meta content="13787749" name="octolytics-actor-id" /><meta content="zdv-1993" name="octolytics-actor-login" /><meta content="75320a462a14f40fb5195762fcc3cc3f75c7135d691c9857ebbf203cb6e5dfb0" name="octolytics-actor-hash" />
<meta content="/&lt;user-name&gt;/&lt;repo-name&gt;/blob/show" data-pjax-transient="true" name="analytics-location" />



  <meta class="js-ga-set" name="dimension1" content="Logged In">



        <meta name="hostname" content="github.com">
    <meta name="user-login" content="zdv-1993">

        <meta name="expected-hostname" content="github.com">
      <meta name="js-proxy-site-detection-payload" content="ZmZjZTI0MDFlOGIxMzRiMDIyODRhODdmZTc2OTEyNDQ3ZjZlOTI1MTNjYWUwNTgzMDIwMTZlZTFmMDE2ZDE4Mnx7InJlbW90ZV9hZGRyZXNzIjoiMTc4Ljc4LjU3LjIyNiIsInJlcXVlc3RfaWQiOiJCMjRFMzlFMjo2NzU3OjczN0NCNEE6NTcyNzA4OUIiLCJ0aW1lc3RhbXAiOjE0NjIxNzU4OTl9">


      <link rel="mask-icon" href="https://assets-cdn.github.com/pinned-octocat.svg" color="#4078c0">
      <link rel="icon" type="image/x-icon" href="https://assets-cdn.github.com/favicon.ico">

    <meta content="db6309c3ff7efcf02cbefe73c6dc05329a01b4d9" name="form-nonce" />

    <meta http-equiv="x-pjax-version" content="a71f90f3b8eda5fe516d8361b5ea9701">
    

      
  <meta name="description" content="datetimepicker - jQuery Plugin Date and Time Picker">
  <meta name="go-import" content="github.com/xdan/datetimepicker git https://github.com/xdan/datetimepicker.git">

  <meta content="794318" name="octolytics-dimension-user_id" /><meta content="xdan" name="octolytics-dimension-user_login" /><meta content="14083440" name="octolytics-dimension-repository_id" /><meta content="xdan/datetimepicker" name="octolytics-dimension-repository_nwo" /><meta content="true" name="octolytics-dimension-repository_public" /><meta content="false" name="octolytics-dimension-repository_is_fork" /><meta content="14083440" name="octolytics-dimension-repository_network_root_id" /><meta content="xdan/datetimepicker" name="octolytics-dimension-repository_network_root_nwo" />
  <link href="https://github.com/xdan/datetimepicker/commits/master.atom" rel="alternate" title="Recent Commits to datetimepicker:master" type="application/atom+xml">


      <link rel="canonical" href="https://github.com/xdan/datetimepicker/blob/master/build/jquery.datetimepicker.min.js" data-pjax-transient>
  </head>


  <body class="logged-in   env-production linux vis-public page-blob">
    <div id="js-pjax-loader-bar" class="pjax-loader-bar"></div>
    <a href="#start-of-content" tabindex="1" class="accessibility-aid js-skip-to-content">Skip to content</a>

    
    
    



        <div class="header header-logged-in true" role="banner">
  <div class="container clearfix">

    <a class="header-logo-invertocat" href="https://github.com/" data-hotkey="g d" aria-label="Homepage" data-ga-click="Header, go to dashboard, icon:logo">
  <svg aria-hidden="true" class="octicon octicon-mark-github" height="28" version="1.1" viewBox="0 0 16 16" width="28"><path d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59 0.4 0.07 0.55-0.17 0.55-0.38 0-0.19-0.01-0.82-0.01-1.49-2.01 0.37-2.53-0.49-2.69-0.94-0.09-0.23-0.48-0.94-0.82-1.13-0.28-0.15-0.68-0.52-0.01-0.53 0.63-0.01 1.08 0.58 1.23 0.82 0.72 1.21 1.87 0.87 2.33 0.66 0.07-0.52 0.28-0.87 0.51-1.07-1.78-0.2-3.64-0.89-3.64-3.95 0-0.87 0.31-1.59 0.82-2.15-0.08-0.2-0.36-1.02 0.08-2.12 0 0 0.67-0.21 2.2 0.82 0.64-0.18 1.32-0.27 2-0.27 0.68 0 1.36 0.09 2 0.27 1.53-1.04 2.2-0.82 2.2-0.82 0.44 1.1 0.16 1.92 0.08 2.12 0.51 0.56 0.82 1.27 0.82 2.15 0 3.07-1.87 3.75-3.65 3.95 0.29 0.25 0.54 0.73 0.54 1.48 0 1.07-0.01 1.93-0.01 2.2 0 0.21 0.15 0.46 0.55 0.38C13.71 14.53 16 11.53 16 8 16 3.58 12.42 0 8 0z"></path></svg>
</a>


        <div class="header-search scoped-search site-scoped-search js-site-search" role="search">
  <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="/xdan/datetimepicker/search" class="js-site-search-form" data-scoped-search-url="/xdan/datetimepicker/search" data-unscoped-search-url="/search" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
    <label class="form-control header-search-wrapper js-chromeless-input-container">
      <div class="header-search-scope">This repository</div>
      <input type="text"
        class="form-control header-search-input js-site-search-focus js-site-search-field is-clearable"
        data-hotkey="s"
        name="q"
        placeholder="Search"
        aria-label="Search this repository"
        data-unscoped-placeholder="Search GitHub"
        data-scoped-placeholder="Search"
        tabindex="1"
        autocapitalize="off">
    </label>
</form></div>


      <ul class="header-nav left" role="navigation">
        <li class="header-nav-item">
          <a href="/pulls" class="js-selected-navigation-item header-nav-link" data-ga-click="Header, click, Nav menu - item:pulls context:user" data-hotkey="g p" data-selected-links="/pulls /pulls/assigned /pulls/mentioned /pulls">
            Pull requests
</a>        </li>
        <li class="header-nav-item">
          <a href="/issues" class="js-selected-navigation-item header-nav-link" data-ga-click="Header, click, Nav menu - item:issues context:user" data-hotkey="g i" data-selected-links="/issues /issues/assigned /issues/mentioned /issues">
            Issues
</a>        </li>
          <li class="header-nav-item">
            <a class="header-nav-link" href="https://gist.github.com/" data-ga-click="Header, go to gist, text:gist">Gist</a>
          </li>
      </ul>

    
<ul class="header-nav user-nav right" id="user-links">
  <li class="header-nav-item">
    
    <a href="/notifications" aria-label="You have no unread notifications" class="header-nav-link notification-indicator tooltipped tooltipped-s js-socket-channel js-notification-indicator" data-channel="notification-changed-v2:13787749" data-ga-click="Header, go to notifications, icon:read" data-hotkey="g n">
        <span class="mail-status "></span>
        <svg aria-hidden="true" class="octicon octicon-bell" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path d="M14 12v1H0v-1l0.73-0.58c0.77-0.77 0.81-2.55 1.19-4.42 0.77-3.77 4.08-5 4.08-5 0-0.55 0.45-1 1-1s1 0.45 1 1c0 0 3.39 1.23 4.16 5 0.38 1.88 0.42 3.66 1.19 4.42l0.66 0.58z m-7 4c1.11 0 2-0.89 2-2H5c0 1.11 0.89 2 2 2z"></path></svg>
</a>
  </li>

  <li class="header-nav-item dropdown js-menu-container">
    <a class="header-nav-link tooltipped tooltipped-s js-menu-target" href="/new"
       aria-label="Create new…"
       data-ga-click="Header, create new, icon:add">
      <svg aria-hidden="true" class="octicon octicon-plus left" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 9H7v5H5V9H0V7h5V2h2v5h5v2z"></path></svg>
      <span class="dropdown-caret"></span>
    </a>

    <div class="dropdown-menu-content js-menu-content">
      <ul class="dropdown-menu dropdown-menu-sw">
        
<a class="dropdown-item" href="/new" data-ga-click="Header, create new repository">
  New repository
</a>


  <a class="dropdown-item" href="/organizations/new" data-ga-click="Header, create new organization">
    New organization
  </a>



  <div class="dropdown-divider"></div>
  <div class="dropdown-header">
    <span title="xdan/datetimepicker">This repository</span>
  </div>
    <a class="dropdown-item" href="/xdan/datetimepicker/issues/new" data-ga-click="Header, create new issue">
      New issue
    </a>

      </ul>
    </div>
  </li>

  <li class="header-nav-item dropdown js-menu-container">
    <a class="header-nav-link name tooltipped tooltipped-sw js-menu-target" href="/zdv-1993"
       aria-label="View profile and more"
       data-ga-click="Header, show menu, icon:avatar">
      <img alt="@zdv-1993" class="avatar" height="20" src="https://avatars0.githubusercontent.com/u/13787749?v=3&amp;s=40" width="20" />
      <span class="dropdown-caret"></span>
    </a>

    <div class="dropdown-menu-content js-menu-content">
      <div class="dropdown-menu  dropdown-menu-sw">
        <div class=" dropdown-header header-nav-current-user css-truncate">
            Signed in as <strong class="css-truncate-target">zdv-1993</strong>

        </div>


        <div class="dropdown-divider"></div>

          <a class="dropdown-item" href="/zdv-1993" data-ga-click="Header, go to profile, text:your profile">
            Your profile
          </a>
        <a class="dropdown-item" href="/stars" data-ga-click="Header, go to starred repos, text:your stars">
          Your stars
        </a>
          <a class="dropdown-item" href="/explore" data-ga-click="Header, go to explore, text:explore">
            Explore
          </a>
          <a class="dropdown-item" href="/integrations" data-ga-click="Header, go to integrations, text:integrations">
            Integrations
          </a>
        <a class="dropdown-item" href="https://help.github.com" data-ga-click="Header, go to help, text:help">
          Help
        </a>


          <div class="dropdown-divider"></div>

          <a class="dropdown-item" href="/settings/profile" data-ga-click="Header, go to settings, icon:settings">
            Settings
          </a>

          <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="/logout" class="logout-form" data-form-nonce="db6309c3ff7efcf02cbefe73c6dc05329a01b4d9" method="post"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /><input name="authenticity_token" type="hidden" value="KNJNq1tZoh00T9vyow2ZAXfVnxCggoHiZfc4gPpv14bMOE1fiGdN8/cf9/dDDM3Uv08dN2pbWsrpwV2ZDDIcYQ==" /></div>
            <button class="dropdown-item dropdown-signout" data-ga-click="Header, sign out, icon:logout">
              Sign out
            </button>
</form>
      </div>
    </div>
  </li>
</ul>


    
  </div>
</div>


      
<div class="flash flash-full js-notice flash-warn">
<div class="container">
      <h4 class="flash-content">
        You don’t have any verified emails.  We recommend <a href="https://github.com/settings/emails">verifying</a> at least one email.
      </h4>
      Email verification helps our support team verify ownership if you lose account access
        and allows you to receive all the notifications you ask for.

















</div>
</div>


    <div id="start-of-content" class="accessibility-aid"></div>

      <div id="js-flash-container">
</div>


    <div role="main" class="main-content">
        <div itemscope itemtype="http://schema.org/SoftwareSourceCode">
    <div id="js-repo-pjax-container" data-pjax-container>
      
<div class="pagehead repohead instapaper_ignore readability-menu experiment-repo-nav">
  <div class="container repohead-details-container">

    

<ul class="pagehead-actions">

  <li>
        <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="/notifications/subscribe" class="js-social-container" data-autosubmit="true" data-form-nonce="db6309c3ff7efcf02cbefe73c6dc05329a01b4d9" data-remote="true" method="post"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /><input name="authenticity_token" type="hidden" value="od/BIGg5Df+zkOn3TSr+Q5ffEq7N2ba2wwHh1baYx2julAurI+e17bbKyzY/Xf6i19RY55WAERysYK9yXfe1gg==" /></div>      <input class="form-control" id="repository_id" name="repository_id" type="hidden" value="14083440" />

        <div class="select-menu js-menu-container js-select-menu">
          <a href="/xdan/datetimepicker/subscription"
            class="btn btn-sm btn-with-count select-menu-button js-menu-target" role="button" tabindex="0" aria-haspopup="true"
            data-ga-click="Repository, click Watch settings, action:blob#show">
            <span class="js-select-button">
              <svg aria-hidden="true" class="octicon octicon-eye" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M8.06 2C3 2 0 8 0 8s3 6 8.06 6c4.94 0 7.94-6 7.94-6S13 2 8.06 2z m-0.06 10c-2.2 0-4-1.78-4-4 0-2.2 1.8-4 4-4 2.22 0 4 1.8 4 4 0 2.22-1.78 4-4 4z m2-4c0 1.11-0.89 2-2 2s-2-0.89-2-2 0.89-2 2-2 2 0.89 2 2z"></path></svg>
              Watch
            </span>
          </a>
          <a class="social-count js-social-count" href="/xdan/datetimepicker/watchers">
            169
          </a>

        <div class="select-menu-modal-holder">
          <div class="select-menu-modal subscription-menu-modal js-menu-content" aria-hidden="true">
            <div class="select-menu-header js-navigation-enable" tabindex="-1">
              <svg aria-label="Close" class="octicon octicon-x js-menu-close" height="16" role="img" version="1.1" viewBox="0 0 12 16" width="12"><path d="M7.48 8l3.75 3.75-1.48 1.48-3.75-3.75-3.75 3.75-1.48-1.48 3.75-3.75L0.77 4.25l1.48-1.48 3.75 3.75 3.75-3.75 1.48 1.48-3.75 3.75z"></path></svg>
              <span class="select-menu-title">Notifications</span>
            </div>

              <div class="select-menu-list js-navigation-container" role="menu">

                <div class="select-menu-item js-navigation-item selected" role="menuitem" tabindex="0">
                  <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
                  <div class="select-menu-item-text">
                    <input checked="checked" id="do_included" name="do" type="radio" value="included" />
                    <span class="select-menu-item-heading">Not watching</span>
                    <span class="description">Be notified when participating or @mentioned.</span>
                    <span class="js-select-button-text hidden-select-button-text">
                      <svg aria-hidden="true" class="octicon octicon-eye" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M8.06 2C3 2 0 8 0 8s3 6 8.06 6c4.94 0 7.94-6 7.94-6S13 2 8.06 2z m-0.06 10c-2.2 0-4-1.78-4-4 0-2.2 1.8-4 4-4 2.22 0 4 1.8 4 4 0 2.22-1.78 4-4 4z m2-4c0 1.11-0.89 2-2 2s-2-0.89-2-2 0.89-2 2-2 2 0.89 2 2z"></path></svg>
                      Watch
                    </span>
                  </div>
                </div>

                <div class="select-menu-item js-navigation-item " role="menuitem" tabindex="0">
                  <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
                  <div class="select-menu-item-text">
                    <input id="do_subscribed" name="do" type="radio" value="subscribed" />
                    <span class="select-menu-item-heading">Watching</span>
                    <span class="description">Be notified of all conversations.</span>
                    <span class="js-select-button-text hidden-select-button-text">
                      <svg aria-hidden="true" class="octicon octicon-eye" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M8.06 2C3 2 0 8 0 8s3 6 8.06 6c4.94 0 7.94-6 7.94-6S13 2 8.06 2z m-0.06 10c-2.2 0-4-1.78-4-4 0-2.2 1.8-4 4-4 2.22 0 4 1.8 4 4 0 2.22-1.78 4-4 4z m2-4c0 1.11-0.89 2-2 2s-2-0.89-2-2 0.89-2 2-2 2 0.89 2 2z"></path></svg>
                      Unwatch
                    </span>
                  </div>
                </div>

                <div class="select-menu-item js-navigation-item " role="menuitem" tabindex="0">
                  <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
                  <div class="select-menu-item-text">
                    <input id="do_ignore" name="do" type="radio" value="ignore" />
                    <span class="select-menu-item-heading">Ignoring</span>
                    <span class="description">Never be notified.</span>
                    <span class="js-select-button-text hidden-select-button-text">
                      <svg aria-hidden="true" class="octicon octicon-mute" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M8 2.81v10.38c0 0.67-0.81 1-1.28 0.53L3 10H1c-0.55 0-1-0.45-1-1V7c0-0.55 0.45-1 1-1h2l3.72-3.72c0.47-0.47 1.28-0.14 1.28 0.53z m7.53 3.22l-1.06-1.06-1.97 1.97-1.97-1.97-1.06 1.06 1.97 1.97-1.97 1.97 1.06 1.06 1.97-1.97 1.97 1.97 1.06-1.06-1.97-1.97 1.97-1.97z"></path></svg>
                      Stop ignoring
                    </span>
                  </div>
                </div>

              </div>

            </div>
          </div>
        </div>
</form>
  </li>

  <li>
    
  <div class="js-toggler-container js-social-container starring-container ">

    <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="/xdan/datetimepicker/unstar" class="starred" data-form-nonce="db6309c3ff7efcf02cbefe73c6dc05329a01b4d9" data-remote="true" method="post"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /><input name="authenticity_token" type="hidden" value="AjqndA0L/xR05OpZOa/kJNMRZ9Z6FbvCh4fes/Hww2Yk+1ryey1DrHbyhIuJWfCvY/xYj90gmRxTc/LU7kLCnw==" /></div>
      <button
        class="btn btn-sm btn-with-count js-toggler-target"
        aria-label="Unstar this repository" title="Unstar xdan/datetimepicker"
        data-ga-click="Repository, click unstar button, action:blob#show; text:Unstar">
        <svg aria-hidden="true" class="octicon octicon-star" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path d="M14 6l-4.9-0.64L7 1 4.9 5.36 0 6l3.6 3.26L2.67 14l4.33-2.33 4.33 2.33L10.4 9.26 14 6z"></path></svg>
        Unstar
      </button>
        <a class="social-count js-social-count" href="/xdan/datetimepicker/stargazers">
          1,705
        </a>
</form>
    <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="/xdan/datetimepicker/star" class="unstarred" data-form-nonce="db6309c3ff7efcf02cbefe73c6dc05329a01b4d9" data-remote="true" method="post"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /><input name="authenticity_token" type="hidden" value="UTUv9xf7lcAG/Rz5Cpzr5AfIjJER1gAjmiFocgWLUqvIksZUkRbQTmKm/KhZFA1yNzo60wEDi6imDiCuOwN+Eg==" /></div>
      <button
        class="btn btn-sm btn-with-count js-toggler-target"
        aria-label="Star this repository" title="Star xdan/datetimepicker"
        data-ga-click="Repository, click star button, action:blob#show; text:Star">
        <svg aria-hidden="true" class="octicon octicon-star" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path d="M14 6l-4.9-0.64L7 1 4.9 5.36 0 6l3.6 3.26L2.67 14l4.33-2.33 4.33 2.33L10.4 9.26 14 6z"></path></svg>
        Star
      </button>
        <a class="social-count js-social-count" href="/xdan/datetimepicker/stargazers">
          1,705
        </a>
</form>  </div>

  </li>

  <li>
          <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="/xdan/datetimepicker/fork" class="btn-with-count" data-form-nonce="db6309c3ff7efcf02cbefe73c6dc05329a01b4d9" method="post"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /><input name="authenticity_token" type="hidden" value="owoCJWRi+XSsZyBne8sm1hdlE06x3Tc1E/Sfd/I25SWm5lhpA5XU6hhiqpRXE0r1oXPjkeIFMWWxAu06ZXITlg==" /></div>
            <button
                type="submit"
                class="btn btn-sm btn-with-count"
                data-ga-click="Repository, show fork modal, action:blob#show; text:Fork"
                title="Fork your own copy of xdan/datetimepicker to your account"
                aria-label="Fork your own copy of xdan/datetimepicker to your account">
              <svg aria-hidden="true" class="octicon octicon-repo-forked" height="16" version="1.1" viewBox="0 0 10 16" width="10"><path d="M8 1c-1.11 0-2 0.89-2 2 0 0.73 0.41 1.38 1 1.72v1.28L5 8 3 6v-1.28c0.59-0.34 1-0.98 1-1.72 0-1.11-0.89-2-2-2S0 1.89 0 3c0 0.73 0.41 1.38 1 1.72v1.78l3 3v1.78c-0.59 0.34-1 0.98-1 1.72 0 1.11 0.89 2 2 2s2-0.89 2-2c0-0.73-0.41-1.38-1-1.72V9.5l3-3V4.72c0.59-0.34 1-0.98 1-1.72 0-1.11-0.89-2-2-2zM2 4.2c-0.66 0-1.2-0.55-1.2-1.2s0.55-1.2 1.2-1.2 1.2 0.55 1.2 1.2-0.55 1.2-1.2 1.2z m3 10c-0.66 0-1.2-0.55-1.2-1.2s0.55-1.2 1.2-1.2 1.2 0.55 1.2 1.2-0.55 1.2-1.2 1.2z m3-10c-0.66 0-1.2-0.55-1.2-1.2s0.55-1.2 1.2-1.2 1.2 0.55 1.2 1.2-0.55 1.2-1.2 1.2z"></path></svg>
              Fork
            </button>
</form>
    <a href="/xdan/datetimepicker/network" class="social-count">
      688
    </a>
  </li>
</ul>

    <h1 class="entry-title public ">
  <svg aria-hidden="true" class="octicon octicon-repo" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M4 9h-1v-1h1v1z m0-3h-1v1h1v-1z m0-2h-1v1h1v-1z m0-2h-1v1h1v-1z m8-1v12c0 0.55-0.45 1-1 1H6v2l-1.5-1.5-1.5 1.5V14H1c-0.55 0-1-0.45-1-1V1C0 0.45 0.45 0 1 0h10c0.55 0 1 0.45 1 1z m-1 10H1v2h2v-1h3v1h5V11z m0-10H2v9h9V1z"></path></svg>
  <span class="author" itemprop="author"><a href="/xdan" class="url fn" rel="author">xdan</a></span><!--
--><span class="path-divider">/</span><!--
--><strong itemprop="name"><a href="/xdan/datetimepicker" data-pjax="#js-repo-pjax-container">datetimepicker</a></strong>

</h1>

  </div>
  <div class="container">
    
<nav class="reponav js-repo-nav js-sidenav-container-pjax"
     itemscope
     itemtype="http://schema.org/BreadcrumbList"
     role="navigation"
     data-pjax="#js-repo-pjax-container">

  <span itemscope itemtype="http://schema.org/ListItem" itemprop="itemListElement">
    <a href="/xdan/datetimepicker" aria-selected="true" class="js-selected-navigation-item selected reponav-item" data-hotkey="g c" data-selected-links="repo_source repo_downloads repo_commits repo_releases repo_tags repo_branches /xdan/datetimepicker" itemprop="url">
      <svg aria-hidden="true" class="octicon octicon-code" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path d="M9.5 3l-1.5 1.5 3.5 3.5L8 11.5l1.5 1.5 4.5-5L9.5 3zM4.5 3L0 8l4.5 5 1.5-1.5L2.5 8l3.5-3.5L4.5 3z"></path></svg>
      <span itemprop="name">Code</span>
      <meta itemprop="position" content="1">
</a>  </span>

    <span itemscope itemtype="http://schema.org/ListItem" itemprop="itemListElement">
      <a href="/xdan/datetimepicker/issues" class="js-selected-navigation-item reponav-item" data-hotkey="g i" data-selected-links="repo_issues repo_labels repo_milestones /xdan/datetimepicker/issues" itemprop="url">
        <svg aria-hidden="true" class="octicon octicon-issue-opened" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path d="M7 2.3c3.14 0 5.7 2.56 5.7 5.7S10.14 13.7 7 13.7 1.3 11.14 1.3 8s2.56-5.7 5.7-5.7m0-1.3C3.14 1 0 4.14 0 8s3.14 7 7 7 7-3.14 7-7S10.86 1 7 1z m1 3H6v5h2V4z m0 6H6v2h2V10z"></path></svg>
        <span itemprop="name">Issues</span>
        <span class="counter">202</span>
        <meta itemprop="position" content="2">
</a>    </span>

  <span itemscope itemtype="http://schema.org/ListItem" itemprop="itemListElement">
    <a href="/xdan/datetimepicker/pulls" class="js-selected-navigation-item reponav-item" data-hotkey="g p" data-selected-links="repo_pulls /xdan/datetimepicker/pulls" itemprop="url">
      <svg aria-hidden="true" class="octicon octicon-git-pull-request" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M11 11.28c0-1.73 0-6.28 0-6.28-0.03-0.78-0.34-1.47-0.94-2.06s-1.28-0.91-2.06-0.94c0 0-1.02 0-1 0V0L4 3l3 3V4h1c0.27 0.02 0.48 0.11 0.69 0.31s0.3 0.42 0.31 0.69v6.28c-0.59 0.34-1 0.98-1 1.72 0 1.11 0.89 2 2 2s2-0.89 2-2c0-0.73-0.41-1.38-1-1.72z m-1 2.92c-0.66 0-1.2-0.55-1.2-1.2s0.55-1.2 1.2-1.2 1.2 0.55 1.2 1.2-0.55 1.2-1.2 1.2zM4 3c0-1.11-0.89-2-2-2S0 1.89 0 3c0 0.73 0.41 1.38 1 1.72 0 1.55 0 5.56 0 6.56-0.59 0.34-1 0.98-1 1.72 0 1.11 0.89 2 2 2s2-0.89 2-2c0-0.73-0.41-1.38-1-1.72V4.72c0.59-0.34 1-0.98 1-1.72z m-0.8 10c0 0.66-0.55 1.2-1.2 1.2s-1.2-0.55-1.2-1.2 0.55-1.2 1.2-1.2 1.2 0.55 1.2 1.2z m-1.2-8.8c-0.66 0-1.2-0.55-1.2-1.2s0.55-1.2 1.2-1.2 1.2 0.55 1.2 1.2-0.55 1.2-1.2 1.2z"></path></svg>
      <span itemprop="name">Pull requests</span>
      <span class="counter">6</span>
      <meta itemprop="position" content="3">
</a>  </span>

    <a href="/xdan/datetimepicker/wiki" class="js-selected-navigation-item reponav-item" data-hotkey="g w" data-selected-links="repo_wiki /xdan/datetimepicker/wiki">
      <svg aria-hidden="true" class="octicon octicon-book" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M2 5h4v1H2v-1z m0 3h4v-1H2v1z m0 2h4v-1H2v1z m11-5H9v1h4v-1z m0 2H9v1h4v-1z m0 2H9v1h4v-1z m2-6v9c0 0.55-0.45 1-1 1H8.5l-1 1-1-1H1c-0.55 0-1-0.45-1-1V3c0-0.55 0.45-1 1-1h5.5l1 1 1-1h5.5c0.55 0 1 0.45 1 1z m-8 0.5l-0.5-0.5H1v9h6V3.5z m7-0.5H8.5l-0.5 0.5v8.5h6V3z"></path></svg>
      Wiki
</a>

  <a href="/xdan/datetimepicker/pulse" class="js-selected-navigation-item reponav-item" data-selected-links="pulse /xdan/datetimepicker/pulse">
    <svg aria-hidden="true" class="octicon octicon-pulse" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path d="M11.5 8L8.8 5.4 6.6 8.5 5.5 1.6 2.38 8H0V10h3.6L4.5 8.2l0.9 5.4L9 8.5l1.6 1.5H14V8H11.5z"></path></svg>
    Pulse
</a>
  <a href="/xdan/datetimepicker/graphs" class="js-selected-navigation-item reponav-item" data-selected-links="repo_graphs repo_contributors /xdan/datetimepicker/graphs">
    <svg aria-hidden="true" class="octicon octicon-graph" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M16 14v1H0V0h1v14h15z m-11-1H3V8h2v5z m4 0H7V3h2v10z m4 0H11V6h2v7z"></path></svg>
    Graphs
</a>

</nav>

  </div>
</div>

<div class="container new-discussion-timeline experiment-repo-nav">
  <div class="repository-content">

    

<a href="/xdan/datetimepicker/blob/5da3d3df15efafc679890d9254452926cde462c0/build/jquery.datetimepicker.min.js" class="hidden js-permalink-shortcut" data-hotkey="y">Permalink</a>

<!-- blob contrib key: blob_contributors:v21:5181bc3bef053fbee7ccbede3f08dc37 -->

<div class="file-navigation js-zeroclipboard-container">
  
<div class="select-menu branch-select-menu js-menu-container js-select-menu left">
  <button class="btn btn-sm select-menu-button js-menu-target css-truncate" data-hotkey="w"
    title="master"
    type="button" aria-label="Switch branches or tags" tabindex="0" aria-haspopup="true">
    <i>Branch:</i>
    <span class="js-select-button css-truncate-target">master</span>
  </button>

  <div class="select-menu-modal-holder js-menu-content js-navigation-container" data-pjax aria-hidden="true">

    <div class="select-menu-modal">
      <div class="select-menu-header">
        <svg aria-label="Close" class="octicon octicon-x js-menu-close" height="16" role="img" version="1.1" viewBox="0 0 12 16" width="12"><path d="M7.48 8l3.75 3.75-1.48 1.48-3.75-3.75-3.75 3.75-1.48-1.48 3.75-3.75L0.77 4.25l1.48-1.48 3.75 3.75 3.75-3.75 1.48 1.48-3.75 3.75z"></path></svg>
        <span class="select-menu-title">Switch branches/tags</span>
      </div>

      <div class="select-menu-filters">
        <div class="select-menu-text-filter">
          <input type="text" aria-label="Filter branches/tags" id="context-commitish-filter-field" class="form-control js-filterable-field js-navigation-enable" placeholder="Filter branches/tags">
        </div>
        <div class="select-menu-tabs">
          <ul>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="branches" data-filter-placeholder="Filter branches/tags" class="js-select-menu-tab" role="tab">Branches</a>
            </li>
            <li class="select-menu-tab">
              <a href="#" data-tab-filter="tags" data-filter-placeholder="Find a tag…" class="js-select-menu-tab" role="tab">Tags</a>
            </li>
          </ul>
        </div>
      </div>

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="branches" role="menu">

        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <a class="select-menu-item js-navigation-item js-navigation-open selected"
               href="/xdan/datetimepicker/blob/master/build/jquery.datetimepicker.min.js"
               data-name="master"
               data-skip-pjax="true"
               rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target js-select-menu-filter-text" title="master">
                master
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/xdan/datetimepicker/blob/revert-218-emptyformatpatch/build/jquery.datetimepicker.min.js"
               data-name="revert-218-emptyformatpatch"
               data-skip-pjax="true"
               rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target js-select-menu-filter-text" title="revert-218-emptyformatpatch">
                revert-218-emptyformatpatch
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
               href="/xdan/datetimepicker/blob/v.1.0.1/build/jquery.datetimepicker.min.js"
               data-name="v.1.0.1"
               data-skip-pjax="true"
               rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target js-select-menu-filter-text" title="v.1.0.1">
                v.1.0.1
              </span>
            </a>
        </div>

          <div class="select-menu-no-results">Nothing to show</div>
      </div>

      <div class="select-menu-list select-menu-tab-bucket js-select-menu-tab-bucket" data-tab-filter="tags">
        <div data-filterable-for="context-commitish-filter-field" data-filterable-type="substring">


            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/v.1.0.1/build/jquery.datetimepicker.min.js"
              data-name="v.1.0.1"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="v.1.0.1">
                v.1.0.1
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.5.3/build/jquery.datetimepicker.min.js"
              data-name="2.5.3"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.5.3">
                2.5.3
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.5.2/build/jquery.datetimepicker.min.js"
              data-name="2.5.2"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.5.2">
                2.5.2
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.5.1/build/jquery.datetimepicker.min.js"
              data-name="2.5.1"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.5.1">
                2.5.1
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.5.0/build/jquery.datetimepicker.min.js"
              data-name="2.5.0"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.5.0">
                2.5.0
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.4.9/build/jquery.datetimepicker.min.js"
              data-name="2.4.9"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.4.9">
                2.4.9
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.4.8/build/jquery.datetimepicker.min.js"
              data-name="2.4.8"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.4.8">
                2.4.8
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.4.7/build/jquery.datetimepicker.min.js"
              data-name="2.4.7"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.4.7">
                2.4.7
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.4.6/build/jquery.datetimepicker.min.js"
              data-name="2.4.6"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.4.6">
                2.4.6
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.4.5/build/jquery.datetimepicker.min.js"
              data-name="2.4.5"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.4.5">
                2.4.5
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.4.4/build/jquery.datetimepicker.min.js"
              data-name="2.4.4"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.4.4">
                2.4.4
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.4.3/build/jquery.datetimepicker.min.js"
              data-name="2.4.3"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.4.3">
                2.4.3
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.4.2/build/jquery.datetimepicker.min.js"
              data-name="2.4.2"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.4.2">
                2.4.2
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.4.1/build/jquery.datetimepicker.min.js"
              data-name="2.4.1"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.4.1">
                2.4.1
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.3.9/build/jquery.datetimepicker.min.js"
              data-name="2.3.9"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.3.9">
                2.3.9
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.3.8/build/jquery.datetimepicker.min.js"
              data-name="2.3.8"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.3.8">
                2.3.8
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.3.7/build/jquery.datetimepicker.min.js"
              data-name="2.3.7"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.3.7">
                2.3.7
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.3.6/build/jquery.datetimepicker.min.js"
              data-name="2.3.6"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.3.6">
                2.3.6
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.3.5/build/jquery.datetimepicker.min.js"
              data-name="2.3.5"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.3.5">
                2.3.5
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.3.4/build/jquery.datetimepicker.min.js"
              data-name="2.3.4"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.3.4">
                2.3.4
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.3.3/build/jquery.datetimepicker.min.js"
              data-name="2.3.3"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.3.3">
                2.3.3
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.3.2/build/jquery.datetimepicker.min.js"
              data-name="2.3.2"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.3.2">
                2.3.2
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.3.1/build/jquery.datetimepicker.min.js"
              data-name="2.3.1"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.3.1">
                2.3.1
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.3.0/build/jquery.datetimepicker.min.js"
              data-name="2.3.0"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.3.0">
                2.3.0
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.2.9/build/jquery.datetimepicker.min.js"
              data-name="2.2.9"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.2.9">
                2.2.9
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.2.8/build/jquery.datetimepicker.min.js"
              data-name="2.2.8"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.2.8">
                2.2.8
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.2.7/build/jquery.datetimepicker.min.js"
              data-name="2.2.7"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.2.7">
                2.2.7
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.2.6/build/jquery.datetimepicker.min.js"
              data-name="2.2.6"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.2.6">
                2.2.6
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.2.5/build/jquery.datetimepicker.min.js"
              data-name="2.2.5"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.2.5">
                2.2.5
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.2.4/build/jquery.datetimepicker.min.js"
              data-name="2.2.4"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.2.4">
                2.2.4
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.2.3/build/jquery.datetimepicker.min.js"
              data-name="2.2.3"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.2.3">
                2.2.3
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.2.2/build/jquery.datetimepicker.min.js"
              data-name="2.2.2"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.2.2">
                2.2.2
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.1.9/build/jquery.datetimepicker.min.js"
              data-name="2.1.9"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.1.9">
                2.1.9
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.1.8/build/jquery.datetimepicker.min.js"
              data-name="2.1.8"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.1.8">
                2.1.8
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.1.7/build/jquery.datetimepicker.min.js"
              data-name="2.1.7"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.1.7">
                2.1.7
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.1.6/build/jquery.datetimepicker.min.js"
              data-name="2.1.6"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.1.6">
                2.1.6
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.1.5/build/jquery.datetimepicker.min.js"
              data-name="2.1.5"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.1.5">
                2.1.5
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.1.4/build/jquery.datetimepicker.min.js"
              data-name="2.1.4"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.1.4">
                2.1.4
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.1.3/build/jquery.datetimepicker.min.js"
              data-name="2.1.3"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.1.3">
                2.1.3
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.1.2/build/jquery.datetimepicker.min.js"
              data-name="2.1.2"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.1.2">
                2.1.2
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.1.1/build/jquery.datetimepicker.min.js"
              data-name="2.1.1"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.1.1">
                2.1.1
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.1.0/build/jquery.datetimepicker.min.js"
              data-name="2.1.0"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.1.0">
                2.1.0
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.0.9/build/jquery.datetimepicker.min.js"
              data-name="2.0.9"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.0.9">
                2.0.9
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.0.8/build/jquery.datetimepicker.min.js"
              data-name="2.0.8"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.0.8">
                2.0.8
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.0.7/build/jquery.datetimepicker.min.js"
              data-name="2.0.7"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.0.7">
                2.0.7
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.0.6/build/jquery.datetimepicker.min.js"
              data-name="2.0.6"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.0.6">
                2.0.6
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.0.5/build/jquery.datetimepicker.min.js"
              data-name="2.0.5"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.0.5">
                2.0.5
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.0.4/build/jquery.datetimepicker.min.js"
              data-name="2.0.4"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.0.4">
                2.0.4
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.0.3/build/jquery.datetimepicker.min.js"
              data-name="2.0.3"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.0.3">
                2.0.3
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.0.2/build/jquery.datetimepicker.min.js"
              data-name="2.0.2"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.0.2">
                2.0.2
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.0.1/build/jquery.datetimepicker.min.js"
              data-name="2.0.1"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.0.1">
                2.0.1
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/2.0.0/build/jquery.datetimepicker.min.js"
              data-name="2.0.0"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="2.0.0">
                2.0.0
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/1.1.1/build/jquery.datetimepicker.min.js"
              data-name="1.1.1"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="1.1.1">
                1.1.1
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/1.1.0/build/jquery.datetimepicker.min.js"
              data-name="1.1.0"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="1.1.0">
                1.1.0
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/1.0.10/build/jquery.datetimepicker.min.js"
              data-name="1.0.10"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="1.0.10">
                1.0.10
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/1.0.9/build/jquery.datetimepicker.min.js"
              data-name="1.0.9"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="1.0.9">
                1.0.9
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/1.0.8/build/jquery.datetimepicker.min.js"
              data-name="1.0.8"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="1.0.8">
                1.0.8
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/1.0.7/build/jquery.datetimepicker.min.js"
              data-name="1.0.7"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="1.0.7">
                1.0.7
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/1.0.5/build/jquery.datetimepicker.min.js"
              data-name="1.0.5"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="1.0.5">
                1.0.5
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/1.0.4/build/jquery.datetimepicker.min.js"
              data-name="1.0.4"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="1.0.4">
                1.0.4
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/1.0.3/build/jquery.datetimepicker.min.js"
              data-name="1.0.3"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="1.0.3">
                1.0.3
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/1.0.2/build/jquery.datetimepicker.min.js"
              data-name="1.0.2"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="1.0.2">
                1.0.2
              </span>
            </a>
            <a class="select-menu-item js-navigation-item js-navigation-open "
              href="/xdan/datetimepicker/tree/1.0.1/build/jquery.datetimepicker.min.js"
              data-name="1.0.1"
              data-skip-pjax="true"
              rel="nofollow">
              <svg aria-hidden="true" class="octicon octicon-check select-menu-item-icon" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M12 5L4 13 0 9l1.5-1.5 2.5 2.5 6.5-6.5 1.5 1.5z"></path></svg>
              <span class="select-menu-item-text css-truncate-target" title="1.0.1">
                1.0.1
              </span>
            </a>
        </div>

        <div class="select-menu-no-results">Nothing to show</div>
      </div>

    </div>
  </div>
</div>

  <div class="btn-group right">
    <a href="/xdan/datetimepicker/find/master"
          class="js-pjax-capture-input btn btn-sm"
          data-pjax
          data-hotkey="t">
      Find file
    </a>
    <button aria-label="Copy file path to clipboard" class="js-zeroclipboard btn btn-sm zeroclipboard-button tooltipped tooltipped-s" data-copied-hint="Copied!" type="button">Copy path</button>
  </div>
  <div class="breadcrumb js-zeroclipboard-target">
    <span class="repo-root js-repo-root"><span class="js-path-segment"><a href="/xdan/datetimepicker"><span>datetimepicker</span></a></span></span><span class="separator">/</span><span class="js-path-segment"><a href="/xdan/datetimepicker/tree/master/build"><span>build</span></a></span><span class="separator">/</span><strong class="final-path">jquery.datetimepicker.min.js</strong>
  </div>
</div>


  <div class="commit-tease">
      <span class="right">
        <a class="commit-tease-sha" href="/xdan/datetimepicker/commit/2266845f52801fb0c810feac3a62bae10d532495" data-pjax>
          2266845
        </a>
        <relative-time datetime="2016-04-06T13:45:46Z">Apr 6, 2016</relative-time>
      </span>
      <div>
        <img alt="@vzverev78" class="avatar" height="20" src="https://avatars1.githubusercontent.com/u/16628863?v=3&amp;s=40" width="20" />
        <a href="/vzverev78" class="user-mention" rel="contributor">vzverev78</a>
          <a href="/xdan/datetimepicker/commit/2266845f52801fb0c810feac3a62bae10d532495" class="message" data-pjax="true" title="show parsed date/time to user after validation
https://github.com/xdan/datetimepicker/issues/425">show parsed date/time to user after validation</a>
      </div>

    <div class="commit-tease-contributors">
      <button type="button" class="btn-link muted-link contributors-toggle" data-facebox="#blob_contributors_box">
        <strong>3</strong>
         contributors
      </button>
          <a class="avatar-link tooltipped tooltipped-s" aria-label="xdan" href="/xdan/datetimepicker/commits/master/build/jquery.datetimepicker.min.js?author=xdan"><img alt="@xdan" class="avatar" height="20" src="https://avatars0.githubusercontent.com/u/794318?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="vzverev78" href="/xdan/datetimepicker/commits/master/build/jquery.datetimepicker.min.js?author=vzverev78"><img alt="@vzverev78" class="avatar" height="20" src="https://avatars1.githubusercontent.com/u/16628863?v=3&amp;s=40" width="20" /> </a>
    <a class="avatar-link tooltipped tooltipped-s" aria-label="malko" href="/xdan/datetimepicker/commits/master/build/jquery.datetimepicker.min.js?author=malko"><img alt="@malko" class="avatar" height="20" src="https://avatars2.githubusercontent.com/u/156753?v=3&amp;s=40" width="20" /> </a>


    </div>

    <div id="blob_contributors_box" style="display:none">
      <h2 class="facebox-header" data-facebox-id="facebox-header">Users who have contributed to this file</h2>
      <ul class="facebox-user-list" data-facebox-id="facebox-description">
          <li class="facebox-user-list-item">
            <img alt="@xdan" height="24" src="https://avatars2.githubusercontent.com/u/794318?v=3&amp;s=48" width="24" />
            <a href="/xdan">xdan</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@vzverev78" height="24" src="https://avatars3.githubusercontent.com/u/16628863?v=3&amp;s=48" width="24" />
            <a href="/vzverev78">vzverev78</a>
          </li>
          <li class="facebox-user-list-item">
            <img alt="@malko" height="24" src="https://avatars0.githubusercontent.com/u/156753?v=3&amp;s=48" width="24" />
            <a href="/malko">malko</a>
          </li>
      </ul>
    </div>
  </div>

<div class="file">
  <div class="file-header">
  <div class="file-actions">

    <div class="btn-group">
      <a href="/xdan/datetimepicker/raw/master/build/jquery.datetimepicker.min.js" class="btn btn-sm " id="raw-url">Raw</a>
        <a href="/xdan/datetimepicker/blame/master/build/jquery.datetimepicker.min.js" class="btn btn-sm js-update-url-with-hash">Blame</a>
      <a href="/xdan/datetimepicker/commits/master/build/jquery.datetimepicker.min.js" class="btn btn-sm " rel="nofollow">History</a>
    </div>


        <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="/xdan/datetimepicker/edit/master/build/jquery.datetimepicker.min.js" class="inline-form js-update-url-with-hash" data-form-nonce="db6309c3ff7efcf02cbefe73c6dc05329a01b4d9" method="post"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /><input name="authenticity_token" type="hidden" value="a6LgtkS41YgAj6EIIWCuRmxg0wJWpTY6QQpSf1csDhRpGEEDoRzo9lY7nTaK/L460zOsFKCtXZe8hZbRIXky4g==" /></div>
          <button class="btn-octicon tooltipped tooltipped-nw" type="submit"
            aria-label="Fork this project and edit the file" data-hotkey="e" data-disable-with>
            <svg aria-hidden="true" class="octicon octicon-pencil" height="16" version="1.1" viewBox="0 0 14 16" width="14"><path d="M0 12v3h3l8-8-3-3L0 12z m3 2H1V12h1v1h1v1z m10.3-9.3l-1.3 1.3-3-3 1.3-1.3c0.39-0.39 1.02-0.39 1.41 0l1.59 1.59c0.39 0.39 0.39 1.02 0 1.41z"></path></svg>
          </button>
</form>        <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="/xdan/datetimepicker/delete/master/build/jquery.datetimepicker.min.js" class="inline-form" data-form-nonce="db6309c3ff7efcf02cbefe73c6dc05329a01b4d9" method="post"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /><input name="authenticity_token" type="hidden" value="XKT6rTCwrHK+Yx6/d7qHoF4HkT0XDe7vktRTm8QBs70lhgkARwyvfHgdyFfgWcAJ8cE1lMhA6XNXaCcrKAwWLg==" /></div>
          <button class="btn-octicon btn-octicon-danger tooltipped tooltipped-nw" type="submit"
            aria-label="Fork this project and delete the file" data-disable-with>
            <svg aria-hidden="true" class="octicon octicon-trashcan" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M10 2H8c0-0.55-0.45-1-1-1H4c-0.55 0-1 0.45-1 1H1c-0.55 0-1 0.45-1 1v1c0 0.55 0.45 1 1 1v9c0 0.55 0.45 1 1 1h7c0.55 0 1-0.45 1-1V5c0.55 0 1-0.45 1-1v-1c0-0.55-0.45-1-1-1z m-1 12H2V5h1v8h1V5h1v8h1V5h1v8h1V5h1v9z m1-10H1v-1h9v1z"></path></svg>
          </button>
</form>  </div>

  <div class="file-info">
      2 lines (2 sloc)
      <span class="file-info-divider"></span>
    46.7 KB
  </div>
</div>

  

  <div itemprop="text" class="blob-wrapper data type-javascript">
      <table class="highlight tab-size js-file-line-container" data-tab-size="8">
      <tr>
        <td id="L1" class="blob-num js-line-number" data-line-number="1"></td>
        <td id="LC1" class="blob-code blob-code-inner js-file-line">!function(e){&quot;function&quot;==typeof define&amp;&amp;define.amd?define([&quot;jquery&quot;,&quot;jquery-mousewheel&quot;],e):&quot;object&quot;==typeof exports?module.exports=e:e(jQuery)}(function(e){&quot;use strict&quot;;function t(e,t,a){this.date=e,this.desc=t,this.style=a}var a={i18n:{ar:{months:[&quot;كانون الثاني&quot;,&quot;شباط&quot;,&quot;آذار&quot;,&quot;نيسان&quot;,&quot;مايو&quot;,&quot;حزيران&quot;,&quot;تموز&quot;,&quot;آب&quot;,&quot;أيلول&quot;,&quot;تشرين الأول&quot;,&quot;تشرين الثاني&quot;,&quot;كانون الأول&quot;],dayOfWeekShort:[&quot;ن&quot;,&quot;ث&quot;,&quot;ع&quot;,&quot;خ&quot;,&quot;ج&quot;,&quot;س&quot;,&quot;ح&quot;],dayOfWeek:[&quot;الأحد&quot;,&quot;الاثنين&quot;,&quot;الثلاثاء&quot;,&quot;الأربعاء&quot;,&quot;الخميس&quot;,&quot;الجمعة&quot;,&quot;السبت&quot;,&quot;الأحد&quot;]},ro:{months:[&quot;Ianuarie&quot;,&quot;Februarie&quot;,&quot;Martie&quot;,&quot;Aprilie&quot;,&quot;Mai&quot;,&quot;Iunie&quot;,&quot;Iulie&quot;,&quot;August&quot;,&quot;Septembrie&quot;,&quot;Octombrie&quot;,&quot;Noiembrie&quot;,&quot;Decembrie&quot;],dayOfWeekShort:[&quot;Du&quot;,&quot;Lu&quot;,&quot;Ma&quot;,&quot;Mi&quot;,&quot;Jo&quot;,&quot;Vi&quot;,&quot;Sâ&quot;],dayOfWeek:[&quot;Duminică&quot;,&quot;Luni&quot;,&quot;Marţi&quot;,&quot;Miercuri&quot;,&quot;Joi&quot;,&quot;Vineri&quot;,&quot;Sâmbătă&quot;]},id:{months:[&quot;Januari&quot;,&quot;Februari&quot;,&quot;Maret&quot;,&quot;April&quot;,&quot;Mei&quot;,&quot;Juni&quot;,&quot;Juli&quot;,&quot;Agustus&quot;,&quot;September&quot;,&quot;Oktober&quot;,&quot;November&quot;,&quot;Desember&quot;],dayOfWeekShort:[&quot;Min&quot;,&quot;Sen&quot;,&quot;Sel&quot;,&quot;Rab&quot;,&quot;Kam&quot;,&quot;Jum&quot;,&quot;Sab&quot;],dayOfWeek:[&quot;Minggu&quot;,&quot;Senin&quot;,&quot;Selasa&quot;,&quot;Rabu&quot;,&quot;Kamis&quot;,&quot;Jumat&quot;,&quot;Sabtu&quot;]},is:{months:[&quot;Janúar&quot;,&quot;Febrúar&quot;,&quot;Mars&quot;,&quot;Apríl&quot;,&quot;Maí&quot;,&quot;Júní&quot;,&quot;Júlí&quot;,&quot;Ágúst&quot;,&quot;September&quot;,&quot;Október&quot;,&quot;Nóvember&quot;,&quot;Desember&quot;],dayOfWeekShort:[&quot;Sun&quot;,&quot;Mán&quot;,&quot;Þrið&quot;,&quot;Mið&quot;,&quot;Fim&quot;,&quot;Fös&quot;,&quot;Lau&quot;],dayOfWeek:[&quot;Sunnudagur&quot;,&quot;Mánudagur&quot;,&quot;Þriðjudagur&quot;,&quot;Miðvikudagur&quot;,&quot;Fimmtudagur&quot;,&quot;Föstudagur&quot;,&quot;Laugardagur&quot;]},bg:{months:[&quot;Януари&quot;,&quot;Февруари&quot;,&quot;Март&quot;,&quot;Април&quot;,&quot;Май&quot;,&quot;Юни&quot;,&quot;Юли&quot;,&quot;Август&quot;,&quot;Септември&quot;,&quot;Октомври&quot;,&quot;Ноември&quot;,&quot;Декември&quot;],dayOfWeekShort:[&quot;Нд&quot;,&quot;Пн&quot;,&quot;Вт&quot;,&quot;Ср&quot;,&quot;Чт&quot;,&quot;Пт&quot;,&quot;Сб&quot;],dayOfWeek:[&quot;Неделя&quot;,&quot;Понеделник&quot;,&quot;Вторник&quot;,&quot;Сряда&quot;,&quot;Четвъртък&quot;,&quot;Петък&quot;,&quot;Събота&quot;]},fa:{months:[&quot;فروردین&quot;,&quot;اردیبهشت&quot;,&quot;خرداد&quot;,&quot;تیر&quot;,&quot;مرداد&quot;,&quot;شهریور&quot;,&quot;مهر&quot;,&quot;آبان&quot;,&quot;آذر&quot;,&quot;دی&quot;,&quot;بهمن&quot;,&quot;اسفند&quot;],dayOfWeekShort:[&quot;یکشنبه&quot;,&quot;دوشنبه&quot;,&quot;سه شنبه&quot;,&quot;چهارشنبه&quot;,&quot;پنجشنبه&quot;,&quot;جمعه&quot;,&quot;شنبه&quot;],dayOfWeek:[&quot;یک‌شنبه&quot;,&quot;دوشنبه&quot;,&quot;سه‌شنبه&quot;,&quot;چهارشنبه&quot;,&quot;پنج‌شنبه&quot;,&quot;جمعه&quot;,&quot;شنبه&quot;,&quot;یک‌شنبه&quot;]},ru:{months:[&quot;Январь&quot;,&quot;Февраль&quot;,&quot;Март&quot;,&quot;Апрель&quot;,&quot;Май&quot;,&quot;Июнь&quot;,&quot;Июль&quot;,&quot;Август&quot;,&quot;Сентябрь&quot;,&quot;Октябрь&quot;,&quot;Ноябрь&quot;,&quot;Декабрь&quot;],dayOfWeekShort:[&quot;Вс&quot;,&quot;Пн&quot;,&quot;Вт&quot;,&quot;Ср&quot;,&quot;Чт&quot;,&quot;Пт&quot;,&quot;Сб&quot;],dayOfWeek:[&quot;Воскресенье&quot;,&quot;Понедельник&quot;,&quot;Вторник&quot;,&quot;Среда&quot;,&quot;Четверг&quot;,&quot;Пятница&quot;,&quot;Суббота&quot;]},uk:{months:[&quot;Січень&quot;,&quot;Лютий&quot;,&quot;Березень&quot;,&quot;Квітень&quot;,&quot;Травень&quot;,&quot;Червень&quot;,&quot;Липень&quot;,&quot;Серпень&quot;,&quot;Вересень&quot;,&quot;Жовтень&quot;,&quot;Листопад&quot;,&quot;Грудень&quot;],dayOfWeekShort:[&quot;Ндл&quot;,&quot;Пнд&quot;,&quot;Втр&quot;,&quot;Срд&quot;,&quot;Чтв&quot;,&quot;Птн&quot;,&quot;Сбт&quot;],dayOfWeek:[&quot;Неділя&quot;,&quot;Понеділок&quot;,&quot;Вівторок&quot;,&quot;Середа&quot;,&quot;Четвер&quot;,&quot;П&#39;ятниця&quot;,&quot;Субота&quot;]},en:{months:[&quot;January&quot;,&quot;February&quot;,&quot;March&quot;,&quot;April&quot;,&quot;May&quot;,&quot;June&quot;,&quot;July&quot;,&quot;August&quot;,&quot;September&quot;,&quot;October&quot;,&quot;November&quot;,&quot;December&quot;],dayOfWeekShort:[&quot;Sun&quot;,&quot;Mon&quot;,&quot;Tue&quot;,&quot;Wed&quot;,&quot;Thu&quot;,&quot;Fri&quot;,&quot;Sat&quot;],dayOfWeek:[&quot;Sunday&quot;,&quot;Monday&quot;,&quot;Tuesday&quot;,&quot;Wednesday&quot;,&quot;Thursday&quot;,&quot;Friday&quot;,&quot;Saturday&quot;]},el:{months:[&quot;Ιανουάριος&quot;,&quot;Φεβρουάριος&quot;,&quot;Μάρτιος&quot;,&quot;Απρίλιος&quot;,&quot;Μάιος&quot;,&quot;Ιούνιος&quot;,&quot;Ιούλιος&quot;,&quot;Αύγουστος&quot;,&quot;Σεπτέμβριος&quot;,&quot;Οκτώβριος&quot;,&quot;Νοέμβριος&quot;,&quot;Δεκέμβριος&quot;],dayOfWeekShort:[&quot;Κυρ&quot;,&quot;Δευ&quot;,&quot;Τρι&quot;,&quot;Τετ&quot;,&quot;Πεμ&quot;,&quot;Παρ&quot;,&quot;Σαβ&quot;],dayOfWeek:[&quot;Κυριακή&quot;,&quot;Δευτέρα&quot;,&quot;Τρίτη&quot;,&quot;Τετάρτη&quot;,&quot;Πέμπτη&quot;,&quot;Παρασκευή&quot;,&quot;Σάββατο&quot;]},de:{months:[&quot;Januar&quot;,&quot;Februar&quot;,&quot;März&quot;,&quot;April&quot;,&quot;Mai&quot;,&quot;Juni&quot;,&quot;Juli&quot;,&quot;August&quot;,&quot;September&quot;,&quot;Oktober&quot;,&quot;November&quot;,&quot;Dezember&quot;],dayOfWeekShort:[&quot;So&quot;,&quot;Mo&quot;,&quot;Di&quot;,&quot;Mi&quot;,&quot;Do&quot;,&quot;Fr&quot;,&quot;Sa&quot;],dayOfWeek:[&quot;Sonntag&quot;,&quot;Montag&quot;,&quot;Dienstag&quot;,&quot;Mittwoch&quot;,&quot;Donnerstag&quot;,&quot;Freitag&quot;,&quot;Samstag&quot;]},nl:{months:[&quot;januari&quot;,&quot;februari&quot;,&quot;maart&quot;,&quot;april&quot;,&quot;mei&quot;,&quot;juni&quot;,&quot;juli&quot;,&quot;augustus&quot;,&quot;september&quot;,&quot;oktober&quot;,&quot;november&quot;,&quot;december&quot;],dayOfWeekShort:[&quot;zo&quot;,&quot;ma&quot;,&quot;di&quot;,&quot;wo&quot;,&quot;do&quot;,&quot;vr&quot;,&quot;za&quot;],dayOfWeek:[&quot;zondag&quot;,&quot;maandag&quot;,&quot;dinsdag&quot;,&quot;woensdag&quot;,&quot;donderdag&quot;,&quot;vrijdag&quot;,&quot;zaterdag&quot;]},tr:{months:[&quot;Ocak&quot;,&quot;Şubat&quot;,&quot;Mart&quot;,&quot;Nisan&quot;,&quot;Mayıs&quot;,&quot;Haziran&quot;,&quot;Temmuz&quot;,&quot;Ağustos&quot;,&quot;Eylül&quot;,&quot;Ekim&quot;,&quot;Kasım&quot;,&quot;Aralık&quot;],dayOfWeekShort:[&quot;Paz&quot;,&quot;Pts&quot;,&quot;Sal&quot;,&quot;Çar&quot;,&quot;Per&quot;,&quot;Cum&quot;,&quot;Cts&quot;],dayOfWeek:[&quot;Pazar&quot;,&quot;Pazartesi&quot;,&quot;Salı&quot;,&quot;Çarşamba&quot;,&quot;Perşembe&quot;,&quot;Cuma&quot;,&quot;Cumartesi&quot;]},fr:{months:[&quot;Janvier&quot;,&quot;Février&quot;,&quot;Mars&quot;,&quot;Avril&quot;,&quot;Mai&quot;,&quot;Juin&quot;,&quot;Juillet&quot;,&quot;Août&quot;,&quot;Septembre&quot;,&quot;Octobre&quot;,&quot;Novembre&quot;,&quot;Décembre&quot;],dayOfWeekShort:[&quot;Dim&quot;,&quot;Lun&quot;,&quot;Mar&quot;,&quot;Mer&quot;,&quot;Jeu&quot;,&quot;Ven&quot;,&quot;Sam&quot;],dayOfWeek:[&quot;dimanche&quot;,&quot;lundi&quot;,&quot;mardi&quot;,&quot;mercredi&quot;,&quot;jeudi&quot;,&quot;vendredi&quot;,&quot;samedi&quot;]},es:{months:[&quot;Enero&quot;,&quot;Febrero&quot;,&quot;Marzo&quot;,&quot;Abril&quot;,&quot;Mayo&quot;,&quot;Junio&quot;,&quot;Julio&quot;,&quot;Agosto&quot;,&quot;Septiembre&quot;,&quot;Octubre&quot;,&quot;Noviembre&quot;,&quot;Diciembre&quot;],dayOfWeekShort:[&quot;Dom&quot;,&quot;Lun&quot;,&quot;Mar&quot;,&quot;Mié&quot;,&quot;Jue&quot;,&quot;Vie&quot;,&quot;Sáb&quot;],dayOfWeek:[&quot;Domingo&quot;,&quot;Lunes&quot;,&quot;Martes&quot;,&quot;Miércoles&quot;,&quot;Jueves&quot;,&quot;Viernes&quot;,&quot;Sábado&quot;]},th:{months:[&quot;มกราคม&quot;,&quot;กุมภาพันธ์&quot;,&quot;มีนาคม&quot;,&quot;เมษายน&quot;,&quot;พฤษภาคม&quot;,&quot;มิถุนายน&quot;,&quot;กรกฎาคม&quot;,&quot;สิงหาคม&quot;,&quot;กันยายน&quot;,&quot;ตุลาคม&quot;,&quot;พฤศจิกายน&quot;,&quot;ธันวาคม&quot;],dayOfWeekShort:[&quot;อา.&quot;,&quot;จ.&quot;,&quot;อ.&quot;,&quot;พ.&quot;,&quot;พฤ.&quot;,&quot;ศ.&quot;,&quot;ส.&quot;],dayOfWeek:[&quot;อาทิตย์&quot;,&quot;จันทร์&quot;,&quot;อังคาร&quot;,&quot;พุธ&quot;,&quot;พฤหัส&quot;,&quot;ศุกร์&quot;,&quot;เสาร์&quot;,&quot;อาทิตย์&quot;]},pl:{months:[&quot;styczeń&quot;,&quot;luty&quot;,&quot;marzec&quot;,&quot;kwiecień&quot;,&quot;maj&quot;,&quot;czerwiec&quot;,&quot;lipiec&quot;,&quot;sierpień&quot;,&quot;wrzesień&quot;,&quot;październik&quot;,&quot;listopad&quot;,&quot;grudzień&quot;],dayOfWeekShort:[&quot;nd&quot;,&quot;pn&quot;,&quot;wt&quot;,&quot;śr&quot;,&quot;cz&quot;,&quot;pt&quot;,&quot;sb&quot;],dayOfWeek:[&quot;niedziela&quot;,&quot;poniedziałek&quot;,&quot;wtorek&quot;,&quot;środa&quot;,&quot;czwartek&quot;,&quot;piątek&quot;,&quot;sobota&quot;]},pt:{months:[&quot;Janeiro&quot;,&quot;Fevereiro&quot;,&quot;Março&quot;,&quot;Abril&quot;,&quot;Maio&quot;,&quot;Junho&quot;,&quot;Julho&quot;,&quot;Agosto&quot;,&quot;Setembro&quot;,&quot;Outubro&quot;,&quot;Novembro&quot;,&quot;Dezembro&quot;],dayOfWeekShort:[&quot;Dom&quot;,&quot;Seg&quot;,&quot;Ter&quot;,&quot;Qua&quot;,&quot;Qui&quot;,&quot;Sex&quot;,&quot;Sab&quot;],dayOfWeek:[&quot;Domingo&quot;,&quot;Segunda&quot;,&quot;Terça&quot;,&quot;Quarta&quot;,&quot;Quinta&quot;,&quot;Sexta&quot;,&quot;Sábado&quot;]},ch:{months:[&quot;一月&quot;,&quot;二月&quot;,&quot;三月&quot;,&quot;四月&quot;,&quot;五月&quot;,&quot;六月&quot;,&quot;七月&quot;,&quot;八月&quot;,&quot;九月&quot;,&quot;十月&quot;,&quot;十一月&quot;,&quot;十二月&quot;],dayOfWeekShort:[&quot;日&quot;,&quot;一&quot;,&quot;二&quot;,&quot;三&quot;,&quot;四&quot;,&quot;五&quot;,&quot;六&quot;]},se:{months:[&quot;Januari&quot;,&quot;Februari&quot;,&quot;Mars&quot;,&quot;April&quot;,&quot;Maj&quot;,&quot;Juni&quot;,&quot;Juli&quot;,&quot;Augusti&quot;,&quot;September&quot;,&quot;Oktober&quot;,&quot;November&quot;,&quot;December&quot;],dayOfWeekShort:[&quot;Sön&quot;,&quot;Mån&quot;,&quot;Tis&quot;,&quot;Ons&quot;,&quot;Tor&quot;,&quot;Fre&quot;,&quot;Lör&quot;]},kr:{months:[&quot;1월&quot;,&quot;2월&quot;,&quot;3월&quot;,&quot;4월&quot;,&quot;5월&quot;,&quot;6월&quot;,&quot;7월&quot;,&quot;8월&quot;,&quot;9월&quot;,&quot;10월&quot;,&quot;11월&quot;,&quot;12월&quot;],dayOfWeekShort:[&quot;일&quot;,&quot;월&quot;,&quot;화&quot;,&quot;수&quot;,&quot;목&quot;,&quot;금&quot;,&quot;토&quot;],dayOfWeek:[&quot;일요일&quot;,&quot;월요일&quot;,&quot;화요일&quot;,&quot;수요일&quot;,&quot;목요일&quot;,&quot;금요일&quot;,&quot;토요일&quot;]},it:{months:[&quot;Gennaio&quot;,&quot;Febbraio&quot;,&quot;Marzo&quot;,&quot;Aprile&quot;,&quot;Maggio&quot;,&quot;Giugno&quot;,&quot;Luglio&quot;,&quot;Agosto&quot;,&quot;Settembre&quot;,&quot;Ottobre&quot;,&quot;Novembre&quot;,&quot;Dicembre&quot;],dayOfWeekShort:[&quot;Dom&quot;,&quot;Lun&quot;,&quot;Mar&quot;,&quot;Mer&quot;,&quot;Gio&quot;,&quot;Ven&quot;,&quot;Sab&quot;],dayOfWeek:[&quot;Domenica&quot;,&quot;Lunedì&quot;,&quot;Martedì&quot;,&quot;Mercoledì&quot;,&quot;Giovedì&quot;,&quot;Venerdì&quot;,&quot;Sabato&quot;]},da:{months:[&quot;January&quot;,&quot;Februar&quot;,&quot;Marts&quot;,&quot;April&quot;,&quot;Maj&quot;,&quot;Juni&quot;,&quot;July&quot;,&quot;August&quot;,&quot;September&quot;,&quot;Oktober&quot;,&quot;November&quot;,&quot;December&quot;],dayOfWeekShort:[&quot;Søn&quot;,&quot;Man&quot;,&quot;Tir&quot;,&quot;Ons&quot;,&quot;Tor&quot;,&quot;Fre&quot;,&quot;Lør&quot;],dayOfWeek:[&quot;søndag&quot;,&quot;mandag&quot;,&quot;tirsdag&quot;,&quot;onsdag&quot;,&quot;torsdag&quot;,&quot;fredag&quot;,&quot;lørdag&quot;]},no:{months:[&quot;Januar&quot;,&quot;Februar&quot;,&quot;Mars&quot;,&quot;April&quot;,&quot;Mai&quot;,&quot;Juni&quot;,&quot;Juli&quot;,&quot;August&quot;,&quot;September&quot;,&quot;Oktober&quot;,&quot;November&quot;,&quot;Desember&quot;],dayOfWeekShort:[&quot;Søn&quot;,&quot;Man&quot;,&quot;Tir&quot;,&quot;Ons&quot;,&quot;Tor&quot;,&quot;Fre&quot;,&quot;Lør&quot;],dayOfWeek:[&quot;Søndag&quot;,&quot;Mandag&quot;,&quot;Tirsdag&quot;,&quot;Onsdag&quot;,&quot;Torsdag&quot;,&quot;Fredag&quot;,&quot;Lørdag&quot;]},ja:{months:[&quot;1月&quot;,&quot;2月&quot;,&quot;3月&quot;,&quot;4月&quot;,&quot;5月&quot;,&quot;6月&quot;,&quot;7月&quot;,&quot;8月&quot;,&quot;9月&quot;,&quot;10月&quot;,&quot;11月&quot;,&quot;12月&quot;],dayOfWeekShort:[&quot;日&quot;,&quot;月&quot;,&quot;火&quot;,&quot;水&quot;,&quot;木&quot;,&quot;金&quot;,&quot;土&quot;],dayOfWeek:[&quot;日曜&quot;,&quot;月曜&quot;,&quot;火曜&quot;,&quot;水曜&quot;,&quot;木曜&quot;,&quot;金曜&quot;,&quot;土曜&quot;]},vi:{months:[&quot;Tháng 1&quot;,&quot;Tháng 2&quot;,&quot;Tháng 3&quot;,&quot;Tháng 4&quot;,&quot;Tháng 5&quot;,&quot;Tháng 6&quot;,&quot;Tháng 7&quot;,&quot;Tháng 8&quot;,&quot;Tháng 9&quot;,&quot;Tháng 10&quot;,&quot;Tháng 11&quot;,&quot;Tháng 12&quot;],dayOfWeekShort:[&quot;CN&quot;,&quot;T2&quot;,&quot;T3&quot;,&quot;T4&quot;,&quot;T5&quot;,&quot;T6&quot;,&quot;T7&quot;],dayOfWeek:[&quot;Chủ nhật&quot;,&quot;Thứ hai&quot;,&quot;Thứ ba&quot;,&quot;Thứ tư&quot;,&quot;Thứ năm&quot;,&quot;Thứ sáu&quot;,&quot;Thứ bảy&quot;]},sl:{months:[&quot;Januar&quot;,&quot;Februar&quot;,&quot;Marec&quot;,&quot;April&quot;,&quot;Maj&quot;,&quot;Junij&quot;,&quot;Julij&quot;,&quot;Avgust&quot;,&quot;September&quot;,&quot;Oktober&quot;,&quot;November&quot;,&quot;December&quot;],dayOfWeekShort:[&quot;Ned&quot;,&quot;Pon&quot;,&quot;Tor&quot;,&quot;Sre&quot;,&quot;Čet&quot;,&quot;Pet&quot;,&quot;Sob&quot;],dayOfWeek:[&quot;Nedelja&quot;,&quot;Ponedeljek&quot;,&quot;Torek&quot;,&quot;Sreda&quot;,&quot;Četrtek&quot;,&quot;Petek&quot;,&quot;Sobota&quot;]},cs:{months:[&quot;Leden&quot;,&quot;Únor&quot;,&quot;Březen&quot;,&quot;Duben&quot;,&quot;Květen&quot;,&quot;Červen&quot;,&quot;Červenec&quot;,&quot;Srpen&quot;,&quot;Září&quot;,&quot;Říjen&quot;,&quot;Listopad&quot;,&quot;Prosinec&quot;],dayOfWeekShort:[&quot;Ne&quot;,&quot;Po&quot;,&quot;Út&quot;,&quot;St&quot;,&quot;Čt&quot;,&quot;Pá&quot;,&quot;So&quot;]},hu:{months:[&quot;Január&quot;,&quot;Február&quot;,&quot;Március&quot;,&quot;Április&quot;,&quot;Május&quot;,&quot;Június&quot;,&quot;Július&quot;,&quot;Augusztus&quot;,&quot;Szeptember&quot;,&quot;Október&quot;,&quot;November&quot;,&quot;December&quot;],dayOfWeekShort:[&quot;Va&quot;,&quot;Hé&quot;,&quot;Ke&quot;,&quot;Sze&quot;,&quot;Cs&quot;,&quot;Pé&quot;,&quot;Szo&quot;],dayOfWeek:[&quot;vasárnap&quot;,&quot;hétfő&quot;,&quot;kedd&quot;,&quot;szerda&quot;,&quot;csütörtök&quot;,&quot;péntek&quot;,&quot;szombat&quot;]},az:{months:[&quot;Yanvar&quot;,&quot;Fevral&quot;,&quot;Mart&quot;,&quot;Aprel&quot;,&quot;May&quot;,&quot;Iyun&quot;,&quot;Iyul&quot;,&quot;Avqust&quot;,&quot;Sentyabr&quot;,&quot;Oktyabr&quot;,&quot;Noyabr&quot;,&quot;Dekabr&quot;],dayOfWeekShort:[&quot;B&quot;,&quot;Be&quot;,&quot;Ça&quot;,&quot;Ç&quot;,&quot;Ca&quot;,&quot;C&quot;,&quot;Ş&quot;],dayOfWeek:[&quot;Bazar&quot;,&quot;Bazar ertəsi&quot;,&quot;Çərşənbə axşamı&quot;,&quot;Çərşənbə&quot;,&quot;Cümə axşamı&quot;,&quot;Cümə&quot;,&quot;Şənbə&quot;]},bs:{months:[&quot;Januar&quot;,&quot;Februar&quot;,&quot;Mart&quot;,&quot;April&quot;,&quot;Maj&quot;,&quot;Jun&quot;,&quot;Jul&quot;,&quot;Avgust&quot;,&quot;Septembar&quot;,&quot;Oktobar&quot;,&quot;Novembar&quot;,&quot;Decembar&quot;],dayOfWeekShort:[&quot;Ned&quot;,&quot;Pon&quot;,&quot;Uto&quot;,&quot;Sri&quot;,&quot;Čet&quot;,&quot;Pet&quot;,&quot;Sub&quot;],dayOfWeek:[&quot;Nedjelja&quot;,&quot;Ponedjeljak&quot;,&quot;Utorak&quot;,&quot;Srijeda&quot;,&quot;Četvrtak&quot;,&quot;Petak&quot;,&quot;Subota&quot;]},ca:{months:[&quot;Gener&quot;,&quot;Febrer&quot;,&quot;Març&quot;,&quot;Abril&quot;,&quot;Maig&quot;,&quot;Juny&quot;,&quot;Juliol&quot;,&quot;Agost&quot;,&quot;Setembre&quot;,&quot;Octubre&quot;,&quot;Novembre&quot;,&quot;Desembre&quot;],dayOfWeekShort:[&quot;Dg&quot;,&quot;Dl&quot;,&quot;Dt&quot;,&quot;Dc&quot;,&quot;Dj&quot;,&quot;Dv&quot;,&quot;Ds&quot;],dayOfWeek:[&quot;Diumenge&quot;,&quot;Dilluns&quot;,&quot;Dimarts&quot;,&quot;Dimecres&quot;,&quot;Dijous&quot;,&quot;Divendres&quot;,&quot;Dissabte&quot;]},&quot;en-GB&quot;:{months:[&quot;January&quot;,&quot;February&quot;,&quot;March&quot;,&quot;April&quot;,&quot;May&quot;,&quot;June&quot;,&quot;July&quot;,&quot;August&quot;,&quot;September&quot;,&quot;October&quot;,&quot;November&quot;,&quot;December&quot;],dayOfWeekShort:[&quot;Sun&quot;,&quot;Mon&quot;,&quot;Tue&quot;,&quot;Wed&quot;,&quot;Thu&quot;,&quot;Fri&quot;,&quot;Sat&quot;],dayOfWeek:[&quot;Sunday&quot;,&quot;Monday&quot;,&quot;Tuesday&quot;,&quot;Wednesday&quot;,&quot;Thursday&quot;,&quot;Friday&quot;,&quot;Saturday&quot;]},et:{months:[&quot;Jaanuar&quot;,&quot;Veebruar&quot;,&quot;Märts&quot;,&quot;Aprill&quot;,&quot;Mai&quot;,&quot;Juuni&quot;,&quot;Juuli&quot;,&quot;August&quot;,&quot;September&quot;,&quot;Oktoober&quot;,&quot;November&quot;,&quot;Detsember&quot;],dayOfWeekShort:[&quot;P&quot;,&quot;E&quot;,&quot;T&quot;,&quot;K&quot;,&quot;N&quot;,&quot;R&quot;,&quot;L&quot;],dayOfWeek:[&quot;Pühapäev&quot;,&quot;Esmaspäev&quot;,&quot;Teisipäev&quot;,&quot;Kolmapäev&quot;,&quot;Neljapäev&quot;,&quot;Reede&quot;,&quot;Laupäev&quot;]},eu:{months:[&quot;Urtarrila&quot;,&quot;Otsaila&quot;,&quot;Martxoa&quot;,&quot;Apirila&quot;,&quot;Maiatza&quot;,&quot;Ekaina&quot;,&quot;Uztaila&quot;,&quot;Abuztua&quot;,&quot;Iraila&quot;,&quot;Urria&quot;,&quot;Azaroa&quot;,&quot;Abendua&quot;],dayOfWeekShort:[&quot;Ig.&quot;,&quot;Al.&quot;,&quot;Ar.&quot;,&quot;Az.&quot;,&quot;Og.&quot;,&quot;Or.&quot;,&quot;La.&quot;],dayOfWeek:[&quot;Igandea&quot;,&quot;Astelehena&quot;,&quot;Asteartea&quot;,&quot;Asteazkena&quot;,&quot;Osteguna&quot;,&quot;Ostirala&quot;,&quot;Larunbata&quot;]},fi:{months:[&quot;Tammikuu&quot;,&quot;Helmikuu&quot;,&quot;Maaliskuu&quot;,&quot;Huhtikuu&quot;,&quot;Toukokuu&quot;,&quot;Kesäkuu&quot;,&quot;Heinäkuu&quot;,&quot;Elokuu&quot;,&quot;Syyskuu&quot;,&quot;Lokakuu&quot;,&quot;Marraskuu&quot;,&quot;Joulukuu&quot;],dayOfWeekShort:[&quot;Su&quot;,&quot;Ma&quot;,&quot;Ti&quot;,&quot;Ke&quot;,&quot;To&quot;,&quot;Pe&quot;,&quot;La&quot;],dayOfWeek:[&quot;sunnuntai&quot;,&quot;maanantai&quot;,&quot;tiistai&quot;,&quot;keskiviikko&quot;,&quot;torstai&quot;,&quot;perjantai&quot;,&quot;lauantai&quot;]},gl:{months:[&quot;Xan&quot;,&quot;Feb&quot;,&quot;Maz&quot;,&quot;Abr&quot;,&quot;Mai&quot;,&quot;Xun&quot;,&quot;Xul&quot;,&quot;Ago&quot;,&quot;Set&quot;,&quot;Out&quot;,&quot;Nov&quot;,&quot;Dec&quot;],dayOfWeekShort:[&quot;Dom&quot;,&quot;Lun&quot;,&quot;Mar&quot;,&quot;Mer&quot;,&quot;Xov&quot;,&quot;Ven&quot;,&quot;Sab&quot;],dayOfWeek:[&quot;Domingo&quot;,&quot;Luns&quot;,&quot;Martes&quot;,&quot;Mércores&quot;,&quot;Xoves&quot;,&quot;Venres&quot;,&quot;Sábado&quot;]},hr:{months:[&quot;Siječanj&quot;,&quot;Veljača&quot;,&quot;Ožujak&quot;,&quot;Travanj&quot;,&quot;Svibanj&quot;,&quot;Lipanj&quot;,&quot;Srpanj&quot;,&quot;Kolovoz&quot;,&quot;Rujan&quot;,&quot;Listopad&quot;,&quot;Studeni&quot;,&quot;Prosinac&quot;],dayOfWeekShort:[&quot;Ned&quot;,&quot;Pon&quot;,&quot;Uto&quot;,&quot;Sri&quot;,&quot;Čet&quot;,&quot;Pet&quot;,&quot;Sub&quot;],dayOfWeek:[&quot;Nedjelja&quot;,&quot;Ponedjeljak&quot;,&quot;Utorak&quot;,&quot;Srijeda&quot;,&quot;Četvrtak&quot;,&quot;Petak&quot;,&quot;Subota&quot;]},ko:{months:[&quot;1월&quot;,&quot;2월&quot;,&quot;3월&quot;,&quot;4월&quot;,&quot;5월&quot;,&quot;6월&quot;,&quot;7월&quot;,&quot;8월&quot;,&quot;9월&quot;,&quot;10월&quot;,&quot;11월&quot;,&quot;12월&quot;],dayOfWeekShort:[&quot;일&quot;,&quot;월&quot;,&quot;화&quot;,&quot;수&quot;,&quot;목&quot;,&quot;금&quot;,&quot;토&quot;],dayOfWeek:[&quot;일요일&quot;,&quot;월요일&quot;,&quot;화요일&quot;,&quot;수요일&quot;,&quot;목요일&quot;,&quot;금요일&quot;,&quot;토요일&quot;]},lt:{months:[&quot;Sausio&quot;,&quot;Vasario&quot;,&quot;Kovo&quot;,&quot;Balandžio&quot;,&quot;Gegužės&quot;,&quot;Birželio&quot;,&quot;Liepos&quot;,&quot;Rugpjūčio&quot;,&quot;Rugsėjo&quot;,&quot;Spalio&quot;,&quot;Lapkričio&quot;,&quot;Gruodžio&quot;],dayOfWeekShort:[&quot;Sek&quot;,&quot;Pir&quot;,&quot;Ant&quot;,&quot;Tre&quot;,&quot;Ket&quot;,&quot;Pen&quot;,&quot;Šeš&quot;],dayOfWeek:[&quot;Sekmadienis&quot;,&quot;Pirmadienis&quot;,&quot;Antradienis&quot;,&quot;Trečiadienis&quot;,&quot;Ketvirtadienis&quot;,&quot;Penktadienis&quot;,&quot;Šeštadienis&quot;]},lv:{months:[&quot;Janvāris&quot;,&quot;Februāris&quot;,&quot;Marts&quot;,&quot;Aprīlis &quot;,&quot;Maijs&quot;,&quot;Jūnijs&quot;,&quot;Jūlijs&quot;,&quot;Augusts&quot;,&quot;Septembris&quot;,&quot;Oktobris&quot;,&quot;Novembris&quot;,&quot;Decembris&quot;],dayOfWeekShort:[&quot;Sv&quot;,&quot;Pr&quot;,&quot;Ot&quot;,&quot;Tr&quot;,&quot;Ct&quot;,&quot;Pk&quot;,&quot;St&quot;],dayOfWeek:[&quot;Svētdiena&quot;,&quot;Pirmdiena&quot;,&quot;Otrdiena&quot;,&quot;Trešdiena&quot;,&quot;Ceturtdiena&quot;,&quot;Piektdiena&quot;,&quot;Sestdiena&quot;]},mk:{months:[&quot;јануари&quot;,&quot;февруари&quot;,&quot;март&quot;,&quot;април&quot;,&quot;мај&quot;,&quot;јуни&quot;,&quot;јули&quot;,&quot;август&quot;,&quot;септември&quot;,&quot;октомври&quot;,&quot;ноември&quot;,&quot;декември&quot;],dayOfWeekShort:[&quot;нед&quot;,&quot;пон&quot;,&quot;вто&quot;,&quot;сре&quot;,&quot;чет&quot;,&quot;пет&quot;,&quot;саб&quot;],dayOfWeek:[&quot;Недела&quot;,&quot;Понеделник&quot;,&quot;Вторник&quot;,&quot;Среда&quot;,&quot;Четврток&quot;,&quot;Петок&quot;,&quot;Сабота&quot;]},mn:{months:[&quot;1-р сар&quot;,&quot;2-р сар&quot;,&quot;3-р сар&quot;,&quot;4-р сар&quot;,&quot;5-р сар&quot;,&quot;6-р сар&quot;,&quot;7-р сар&quot;,&quot;8-р сар&quot;,&quot;9-р сар&quot;,&quot;10-р сар&quot;,&quot;11-р сар&quot;,&quot;12-р сар&quot;],dayOfWeekShort:[&quot;Дав&quot;,&quot;Мяг&quot;,&quot;Лха&quot;,&quot;Пүр&quot;,&quot;Бсн&quot;,&quot;Бям&quot;,&quot;Ням&quot;],dayOfWeek:[&quot;Даваа&quot;,&quot;Мягмар&quot;,&quot;Лхагва&quot;,&quot;Пүрэв&quot;,&quot;Баасан&quot;,&quot;Бямба&quot;,&quot;Ням&quot;]},&quot;pt-BR&quot;:{months:[&quot;Janeiro&quot;,&quot;Fevereiro&quot;,&quot;Março&quot;,&quot;Abril&quot;,&quot;Maio&quot;,&quot;Junho&quot;,&quot;Julho&quot;,&quot;Agosto&quot;,&quot;Setembro&quot;,&quot;Outubro&quot;,&quot;Novembro&quot;,&quot;Dezembro&quot;],dayOfWeekShort:[&quot;Dom&quot;,&quot;Seg&quot;,&quot;Ter&quot;,&quot;Qua&quot;,&quot;Qui&quot;,&quot;Sex&quot;,&quot;Sáb&quot;],dayOfWeek:[&quot;Domingo&quot;,&quot;Segunda&quot;,&quot;Terça&quot;,&quot;Quarta&quot;,&quot;Quinta&quot;,&quot;Sexta&quot;,&quot;Sábado&quot;]},sk:{months:[&quot;Január&quot;,&quot;Február&quot;,&quot;Marec&quot;,&quot;Apríl&quot;,&quot;Máj&quot;,&quot;Jún&quot;,&quot;Júl&quot;,&quot;August&quot;,&quot;September&quot;,&quot;Október&quot;,&quot;November&quot;,&quot;December&quot;],dayOfWeekShort:[&quot;Ne&quot;,&quot;Po&quot;,&quot;Ut&quot;,&quot;St&quot;,&quot;Št&quot;,&quot;Pi&quot;,&quot;So&quot;],dayOfWeek:[&quot;Nedeľa&quot;,&quot;Pondelok&quot;,&quot;Utorok&quot;,&quot;Streda&quot;,&quot;Štvrtok&quot;,&quot;Piatok&quot;,&quot;Sobota&quot;]},sq:{months:[&quot;Janar&quot;,&quot;Shkurt&quot;,&quot;Mars&quot;,&quot;Prill&quot;,&quot;Maj&quot;,&quot;Qershor&quot;,&quot;Korrik&quot;,&quot;Gusht&quot;,&quot;Shtator&quot;,&quot;Tetor&quot;,&quot;Nëntor&quot;,&quot;Dhjetor&quot;],dayOfWeekShort:[&quot;Die&quot;,&quot;Hën&quot;,&quot;Mar&quot;,&quot;Mër&quot;,&quot;Enj&quot;,&quot;Pre&quot;,&quot;Shtu&quot;],dayOfWeek:[&quot;E Diel&quot;,&quot;E Hënë&quot;,&quot;E Martē&quot;,&quot;E Mërkurë&quot;,&quot;E Enjte&quot;,&quot;E Premte&quot;,&quot;E Shtunë&quot;]},&quot;sr-YU&quot;:{months:[&quot;Januar&quot;,&quot;Februar&quot;,&quot;Mart&quot;,&quot;April&quot;,&quot;Maj&quot;,&quot;Jun&quot;,&quot;Jul&quot;,&quot;Avgust&quot;,&quot;Septembar&quot;,&quot;Oktobar&quot;,&quot;Novembar&quot;,&quot;Decembar&quot;],dayOfWeekShort:[&quot;Ned&quot;,&quot;Pon&quot;,&quot;Uto&quot;,&quot;Sre&quot;,&quot;čet&quot;,&quot;Pet&quot;,&quot;Sub&quot;],dayOfWeek:[&quot;Nedelja&quot;,&quot;Ponedeljak&quot;,&quot;Utorak&quot;,&quot;Sreda&quot;,&quot;Četvrtak&quot;,&quot;Petak&quot;,&quot;Subota&quot;]},sr:{months:[&quot;јануар&quot;,&quot;фебруар&quot;,&quot;март&quot;,&quot;април&quot;,&quot;мај&quot;,&quot;јун&quot;,&quot;јул&quot;,&quot;август&quot;,&quot;септембар&quot;,&quot;октобар&quot;,&quot;новембар&quot;,&quot;децембар&quot;],dayOfWeekShort:[&quot;нед&quot;,&quot;пон&quot;,&quot;уто&quot;,&quot;сре&quot;,&quot;чет&quot;,&quot;пет&quot;,&quot;суб&quot;],dayOfWeek:[&quot;Недеља&quot;,&quot;Понедељак&quot;,&quot;Уторак&quot;,&quot;Среда&quot;,&quot;Четвртак&quot;,&quot;Петак&quot;,&quot;Субота&quot;]},sv:{months:[&quot;Januari&quot;,&quot;Februari&quot;,&quot;Mars&quot;,&quot;April&quot;,&quot;Maj&quot;,&quot;Juni&quot;,&quot;Juli&quot;,&quot;Augusti&quot;,&quot;September&quot;,&quot;Oktober&quot;,&quot;November&quot;,&quot;December&quot;],dayOfWeekShort:[&quot;Sön&quot;,&quot;Mån&quot;,&quot;Tis&quot;,&quot;Ons&quot;,&quot;Tor&quot;,&quot;Fre&quot;,&quot;Lör&quot;],dayOfWeek:[&quot;Söndag&quot;,&quot;Måndag&quot;,&quot;Tisdag&quot;,&quot;Onsdag&quot;,&quot;Torsdag&quot;,&quot;Fredag&quot;,&quot;Lördag&quot;]},&quot;zh-TW&quot;:{months:[&quot;一月&quot;,&quot;二月&quot;,&quot;三月&quot;,&quot;四月&quot;,&quot;五月&quot;,&quot;六月&quot;,&quot;七月&quot;,&quot;八月&quot;,&quot;九月&quot;,&quot;十月&quot;,&quot;十一月&quot;,&quot;十二月&quot;],dayOfWeekShort:[&quot;日&quot;,&quot;一&quot;,&quot;二&quot;,&quot;三&quot;,&quot;四&quot;,&quot;五&quot;,&quot;六&quot;],dayOfWeek:[&quot;星期日&quot;,&quot;星期一&quot;,&quot;星期二&quot;,&quot;星期三&quot;,&quot;星期四&quot;,&quot;星期五&quot;,&quot;星期六&quot;]},zh:{months:[&quot;一月&quot;,&quot;二月&quot;,&quot;三月&quot;,&quot;四月&quot;,&quot;五月&quot;,&quot;六月&quot;,&quot;七月&quot;,&quot;八月&quot;,&quot;九月&quot;,&quot;十月&quot;,&quot;十一月&quot;,&quot;十二月&quot;],dayOfWeekShort:[&quot;日&quot;,&quot;一&quot;,&quot;二&quot;,&quot;三&quot;,&quot;四&quot;,&quot;五&quot;,&quot;六&quot;],dayOfWeek:[&quot;星期日&quot;,&quot;星期一&quot;,&quot;星期二&quot;,&quot;星期三&quot;,&quot;星期四&quot;,&quot;星期五&quot;,&quot;星期六&quot;]},he:{months:[&quot;ינואר&quot;,&quot;פברואר&quot;,&quot;מרץ&quot;,&quot;אפריל&quot;,&quot;מאי&quot;,&quot;יוני&quot;,&quot;יולי&quot;,&quot;אוגוסט&quot;,&quot;ספטמבר&quot;,&quot;אוקטובר&quot;,&quot;נובמבר&quot;,&quot;דצמבר&quot;],dayOfWeekShort:[&quot;א&#39;&quot;,&quot;ב&#39;&quot;,&quot;ג&#39;&quot;,&quot;ד&#39;&quot;,&quot;ה&#39;&quot;,&quot;ו&#39;&quot;,&quot;שבת&quot;],dayOfWeek:[&quot;ראשון&quot;,&quot;שני&quot;,&quot;שלישי&quot;,&quot;רביעי&quot;,&quot;חמישי&quot;,&quot;שישי&quot;,&quot;שבת&quot;,&quot;ראשון&quot;]},hy:{months:[&quot;Հունվար&quot;,&quot;Փետրվար&quot;,&quot;Մարտ&quot;,&quot;Ապրիլ&quot;,&quot;Մայիս&quot;,&quot;Հունիս&quot;,&quot;Հուլիս&quot;,&quot;Օգոստոս&quot;,&quot;Սեպտեմբեր&quot;,&quot;Հոկտեմբեր&quot;,&quot;Նոյեմբեր&quot;,&quot;Դեկտեմբեր&quot;],dayOfWeekShort:[&quot;Կի&quot;,&quot;Երկ&quot;,&quot;Երք&quot;,&quot;Չոր&quot;,&quot;Հնգ&quot;,&quot;Ուրբ&quot;,&quot;Շբթ&quot;],dayOfWeek:[&quot;Կիրակի&quot;,&quot;Երկուշաբթի&quot;,&quot;Երեքշաբթի&quot;,&quot;Չորեքշաբթի&quot;,&quot;Հինգշաբթի&quot;,&quot;Ուրբաթ&quot;,&quot;Շաբաթ&quot;]},kg:{months:[&quot;Үчтүн айы&quot;,&quot;Бирдин айы&quot;,&quot;Жалган Куран&quot;,&quot;Чын Куран&quot;,&quot;Бугу&quot;,&quot;Кулжа&quot;,&quot;Теке&quot;,&quot;Баш Оона&quot;,&quot;Аяк Оона&quot;,&quot;Тогуздун айы&quot;,&quot;Жетинин айы&quot;,&quot;Бештин айы&quot;],dayOfWeekShort:[&quot;Жек&quot;,&quot;Дүй&quot;,&quot;Шей&quot;,&quot;Шар&quot;,&quot;Бей&quot;,&quot;Жум&quot;,&quot;Ише&quot;],dayOfWeek:[&quot;Жекшемб&quot;,&quot;Дүйшөмб&quot;,&quot;Шейшемб&quot;,&quot;Шаршемб&quot;,&quot;Бейшемби&quot;,&quot;Жума&quot;,&quot;Ишенб&quot;]},rm:{months:[&quot;Schaner&quot;,&quot;Favrer&quot;,&quot;Mars&quot;,&quot;Avrigl&quot;,&quot;Matg&quot;,&quot;Zercladur&quot;,&quot;Fanadur&quot;,&quot;Avust&quot;,&quot;Settember&quot;,&quot;October&quot;,&quot;November&quot;,&quot;December&quot;],dayOfWeekShort:[&quot;Du&quot;,&quot;Gli&quot;,&quot;Ma&quot;,&quot;Me&quot;,&quot;Gie&quot;,&quot;Ve&quot;,&quot;So&quot;],dayOfWeek:[&quot;Dumengia&quot;,&quot;Glindesdi&quot;,&quot;Mardi&quot;,&quot;Mesemna&quot;,&quot;Gievgia&quot;,&quot;Venderdi&quot;,&quot;Sonda&quot;]},ka:{months:[&quot;იანვარი&quot;,&quot;თებერვალი&quot;,&quot;მარტი&quot;,&quot;აპრილი&quot;,&quot;მაისი&quot;,&quot;ივნისი&quot;,&quot;ივლისი&quot;,&quot;აგვისტო&quot;,&quot;სექტემბერი&quot;,&quot;ოქტომბერი&quot;,&quot;ნოემბერი&quot;,&quot;დეკემბერი&quot;],dayOfWeekShort:[&quot;კვ&quot;,&quot;ორშ&quot;,&quot;სამშ&quot;,&quot;ოთხ&quot;,&quot;ხუთ&quot;,&quot;პარ&quot;,&quot;შაბ&quot;],dayOfWeek:[&quot;კვირა&quot;,&quot;ორშაბათი&quot;,&quot;სამშაბათი&quot;,&quot;ოთხშაბათი&quot;,&quot;ხუთშაბათი&quot;,&quot;პარასკევი&quot;,&quot;შაბათი&quot;]}},value:&quot;&quot;,rtl:!1,format:&quot;Y/m/d H:i&quot;,formatTime:&quot;H:i&quot;,formatDate:&quot;Y/m/d&quot;,startDate:!1,step:60,monthChangeSpinner:!0,closeOnDateSelect:!1,closeOnTimeSelect:!0,closeOnWithoutClick:!0,closeOnInputClick:!0,timepicker:!0,datepicker:!0,weeks:!1,defaultTime:!1,defaultDate:!1,minDate:!1,maxDate:!1,minTime:!1,maxTime:!1,disabledMinTime:!1,disabledMaxTime:!1,allowTimes:[],opened:!1,initTime:!0,inline:!1,theme:&quot;&quot;,onSelectDate:function(){},onSelectTime:function(){},onChangeMonth:function(){},onGetWeekOfYear:function(){},onChangeYear:function(){},onChangeDateTime:function(){},onShow:function(){},onClose:function(){},onGenerate:function(){},withoutCopyright:!0,inverseButton:!1,hours12:!1,next:&quot;xdsoft_next&quot;,prev:&quot;xdsoft_prev&quot;,dayOfWeekStart:0,parentID:&quot;body&quot;,timeHeightInTimePicker:25,timepickerScrollbar:!0,todayButton:!0,prevButton:!0,nextButton:!0,defaultSelect:!0,scrollMonth:!0,scrollTime:!0,scrollInput:!0,lazyInit:!1,mask:!1,validateOnBlur:!0,allowBlank:!0,yearStart:1950,yearEnd:2050,monthStart:0,monthEnd:11,style:&quot;&quot;,id:&quot;&quot;,fixed:!1,roundTime:&quot;round&quot;,className:&quot;&quot;,weekends:[],highlightedDates:[],highlightedPeriods:[],allowDates:[],allowDateRe:null,disabledDates:[],disabledWeekDays:[],yearOffset:0,beforeShowDay:null,enterLikeTab:!0,showApplyButton:!1},o=null,r=&quot;en&quot;,n=&quot;en&quot;,i={meridiem:[&quot;AM&quot;,&quot;PM&quot;]},s=function(){var t=a.i18n[n],r={days:t.dayOfWeek,daysShort:t.dayOfWeekShort,months:t.months,monthsShort:e.map(t.months,function(e){return e.substring(0,3)})};o=new DateFormatter({dateSettings:e.extend({},i,r)})};e.datetimepicker={setLocale:function(e){var t=a.i18n[e]?e:r;n!=t&amp;&amp;(n=t,s())},setDateFormatter:function(e){o=e},RFC_2822:&quot;D, d M Y H:i:s O&quot;,ATOM:&quot;Y-m-dTH:i:sP&quot;,ISO_8601:&quot;Y-m-dTH:i:sO&quot;,RFC_822:&quot;D, d M y H:i:s O&quot;,RFC_850:&quot;l, d-M-y H:i:s T&quot;,RFC_1036:&quot;D, d M y H:i:s O&quot;,RFC_1123:&quot;D, d M Y H:i:s O&quot;,RSS:&quot;D, d M Y H:i:s O&quot;,W3C:&quot;Y-m-dTH:i:sP&quot;},s(),window.getComputedStyle||(window.getComputedStyle=function(e){return this.el=e,this.getPropertyValue=function(t){var a=/(\-([a-z]){1})/g;return&quot;float&quot;===t&amp;&amp;(t=&quot;styleFloat&quot;),a.test(t)&amp;&amp;(t=t.replace(a,function(e,t,a){return a.toUpperCase()})),e.currentStyle[t]||null},this}),Array.prototype.indexOf||(Array.prototype.indexOf=function(e,t){var a,o;for(a=t||0,o=this.length;o&gt;a;a+=1)if(this[a]===e)return a;return-1}),Date.prototype.countDaysInMonth=function(){return new Date(this.getFullYear(),this.getMonth()+1,0).getDate()},e.fn.xdsoftScroller=function(t){return this.each(function(){var a,o,r,n,i,s=e(this),d=function(e){var t,a={x:0,y:0};return&quot;touchstart&quot;===e.type||&quot;touchmove&quot;===e.type||&quot;touchend&quot;===e.type||&quot;touchcancel&quot;===e.type?(t=e.originalEvent.touches[0]||e.originalEvent.changedTouches[0],a.x=t.clientX,a.y=t.clientY):(&quot;mousedown&quot;===e.type||&quot;mouseup&quot;===e.type||&quot;mousemove&quot;===e.type||&quot;mouseover&quot;===e.type||&quot;mouseout&quot;===e.type||&quot;mouseenter&quot;===e.type||&quot;mouseleave&quot;===e.type)&amp;&amp;(a.x=e.clientX,a.y=e.clientY),a},u=100,l=!1,f=0,c=0,m=0,h=!1,g=0,p=function(){};return&quot;hide&quot;===t?void s.find(&quot;.xdsoft_scrollbar&quot;).hide():(e(this).hasClass(&quot;xdsoft_scroller_box&quot;)||(a=s.children().eq(0),o=s[0].clientHeight,r=a[0].offsetHeight,n=e(&#39;&lt;div class=&quot;xdsoft_scrollbar&quot;&gt;&lt;/div&gt;&#39;),i=e(&#39;&lt;div class=&quot;xdsoft_scroller&quot;&gt;&lt;/div&gt;&#39;),n.append(i),s.addClass(&quot;xdsoft_scroller_box&quot;).append(n),p=function(e){var t=d(e).y-f+g;0&gt;t&amp;&amp;(t=0),t+i[0].offsetHeight&gt;m&amp;&amp;(t=m-i[0].offsetHeight),s.trigger(&quot;scroll_element.xdsoft_scroller&quot;,[u?t/u:0])},i.on(&quot;touchstart.xdsoft_scroller mousedown.xdsoft_scroller&quot;,function(a){o||s.trigger(&quot;resize_scroll.xdsoft_scroller&quot;,[t]),f=d(a).y,g=parseInt(i.css(&quot;margin-top&quot;),10),m=n[0].offsetHeight,&quot;mousedown&quot;===a.type||&quot;touchstart&quot;===a.type?(document&amp;&amp;e(document.body).addClass(&quot;xdsoft_noselect&quot;),e([document.body,window]).on(&quot;touchend mouseup.xdsoft_scroller&quot;,function r(){e([document.body,window]).off(&quot;touchend mouseup.xdsoft_scroller&quot;,r).off(&quot;mousemove.xdsoft_scroller&quot;,p).removeClass(&quot;xdsoft_noselect&quot;)}),e(document.body).on(&quot;mousemove.xdsoft_scroller&quot;,p)):(h=!0,a.stopPropagation(),a.preventDefault())}).on(&quot;touchmove&quot;,function(e){h&amp;&amp;(e.preventDefault(),p(e))}).on(&quot;touchend touchcancel&quot;,function(){h=!1,g=0}),s.on(&quot;scroll_element.xdsoft_scroller&quot;,function(e,t){o||s.trigger(&quot;resize_scroll.xdsoft_scroller&quot;,[t,!0]),t=t&gt;1?1:0&gt;t||isNaN(t)?0:t,i.css(&quot;margin-top&quot;,u*t),setTimeout(function(){a.css(&quot;marginTop&quot;,-parseInt((a[0].offsetHeight-o)*t,10))},10)}).on(&quot;resize_scroll.xdsoft_scroller&quot;,function(e,t,d){var l,f;o=s[0].clientHeight,r=a[0].offsetHeight,l=o/r,f=l*n[0].offsetHeight,l&gt;1?i.hide():(i.show(),i.css(&quot;height&quot;,parseInt(f&gt;10?f:10,10)),u=n[0].offsetHeight-i[0].offsetHeight,d!==!0&amp;&amp;s.trigger(&quot;scroll_element.xdsoft_scroller&quot;,[t||Math.abs(parseInt(a.css(&quot;marginTop&quot;),10))/(r-o)]))}),s.on(&quot;mousewheel&quot;,function(e){var t=Math.abs(parseInt(a.css(&quot;marginTop&quot;),10));return t-=20*e.deltaY,0&gt;t&amp;&amp;(t=0),s.trigger(&quot;scroll_element.xdsoft_scroller&quot;,[t/(r-o)]),e.stopPropagation(),!1}),s.on(&quot;touchstart&quot;,function(e){l=d(e),c=Math.abs(parseInt(a.css(&quot;marginTop&quot;),10))}),s.on(&quot;touchmove&quot;,function(e){if(l){e.preventDefault();var t=d(e);s.trigger(&quot;scroll_element.xdsoft_scroller&quot;,[(c-(t.y-l.y))/(r-o)])}}),s.on(&quot;touchend touchcancel&quot;,function(){l=!1,c=0})),void s.trigger(&quot;resize_scroll.xdsoft_scroller&quot;,[t]))})},e.fn.datetimepicker=function(r,i){var s,d,u=this,l=48,f=57,c=96,m=105,h=17,g=46,p=13,y=27,k=8,b=37,x=38,v=39,D=40,T=9,S=116,O=65,M=67,w=86,_=90,W=89,F=!1,C=e.isPlainObject(r)||!r?e.extend(!0,{},a,r):e.extend(!0,{},a),A=0,P=function(e){e.on(&quot;open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart&quot;,function t(){e.is(&quot;:disabled&quot;)||e.data(&quot;xdsoft_datetimepicker&quot;)||(clearTimeout(A),A=setTimeout(function(){e.data(&quot;xdsoft_datetimepicker&quot;)||s(e),e.off(&quot;open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart&quot;,t).trigger(&quot;open.xdsoft&quot;)},100))})};return s=function(a){function i(){var e,t=!1;return C.startDate?t=J.strToDate(C.startDate):(t=C.value||(a&amp;&amp;a.val&amp;&amp;a.val()?a.val():&quot;&quot;),t?t=J.strToDateTime(t):C.defaultDate&amp;&amp;(t=J.strToDateTime(C.defaultDate),C.defaultTime&amp;&amp;(e=J.strtotime(C.defaultTime),t.setHours(e.getHours()),t.setMinutes(e.getMinutes())))),t&amp;&amp;J.isValidDate(t)?Y.data(&quot;changed&quot;,!0):t=&quot;&quot;,t||0}function s(t){var o=function(e,t){var a=e.replace(/([\[\]\/\{\}\(\)\-\.\+]{1})/g,&quot;\\$1&quot;).replace(/_/g,&quot;{digit+}&quot;).replace(/([0-9]{1})/g,&quot;{digit$1}&quot;).replace(/\{digit([0-9]{1})\}/g,&quot;[0-$1_]{1}&quot;).replace(/\{digit[\+]\}/g,&quot;[0-9_]{1}&quot;);return new RegExp(a).test(t)},r=function(e){try{if(document.selection&amp;&amp;document.selection.createRange){var t=document.selection.createRange();return t.getBookmark().charCodeAt(2)-2}if(e.setSelectionRange)return e.selectionStart}catch(a){return 0}},n=function(e,t){if(e=&quot;string&quot;==typeof e||e instanceof String?document.getElementById(e):e,!e)return!1;if(e.createTextRange){var a=e.createTextRange();return a.collapse(!0),a.moveEnd(&quot;character&quot;,t),a.moveStart(&quot;character&quot;,t),a.select(),!0}return e.setSelectionRange?(e.setSelectionRange(t,t),!0):!1};t.mask&amp;&amp;a.off(&quot;keydown.xdsoft&quot;),t.mask===!0&amp;&amp;(t.mask=&quot;undefined&quot;!=typeof moment?t.format.replace(/Y{4}/g,&quot;9999&quot;).replace(/Y{2}/g,&quot;99&quot;).replace(/M{2}/g,&quot;19&quot;).replace(/D{2}/g,&quot;39&quot;).replace(/H{2}/g,&quot;29&quot;).replace(/m{2}/g,&quot;59&quot;).replace(/s{2}/g,&quot;59&quot;):t.format.replace(/Y/g,&quot;9999&quot;).replace(/F/g,&quot;9999&quot;).replace(/m/g,&quot;19&quot;).replace(/d/g,&quot;39&quot;).replace(/H/g,&quot;29&quot;).replace(/i/g,&quot;59&quot;).replace(/s/g,&quot;59&quot;)),&quot;string&quot;===e.type(t.mask)&amp;&amp;(o(t.mask,a.val())||(a.val(t.mask.replace(/[0-9]/g,&quot;_&quot;)),n(a[0],0)),a.on(&quot;keydown.xdsoft&quot;,function(i){var s,d,u=this.value,C=i.which;if(C&gt;=l&amp;&amp;f&gt;=C||C&gt;=c&amp;&amp;m&gt;=C||C===k||C===g){for(s=r(this),d=C!==k&amp;&amp;C!==g?String.fromCharCode(C&gt;=c&amp;&amp;m&gt;=C?C-l:C):&quot;_&quot;,C!==k&amp;&amp;C!==g||!s||(s-=1,d=&quot;_&quot;);/[^0-9_]/.test(t.mask.substr(s,1))&amp;&amp;s&lt;t.mask.length&amp;&amp;s&gt;0;)s+=C===k||C===g?-1:1;if(u=u.substr(0,s)+d+u.substr(s+1),&quot;&quot;===e.trim(u))u=t.mask.replace(/[0-9]/g,&quot;_&quot;);else if(s===t.mask.length)return i.preventDefault(),!1;for(s+=C===k||C===g?0:1;/[^0-9_]/.test(t.mask.substr(s,1))&amp;&amp;s&lt;t.mask.length&amp;&amp;s&gt;0;)s+=C===k||C===g?-1:1;o(t.mask,u)?(this.value=u,n(this,s)):&quot;&quot;===e.trim(u)?this.value=t.mask.replace(/[0-9]/g,&quot;_&quot;):a.trigger(&quot;error_input.xdsoft&quot;)}else if(-1!==[O,M,w,_,W].indexOf(C)&amp;&amp;F||-1!==[y,x,D,b,v,S,h,T,p].indexOf(C))return!0;return i.preventDefault(),!1}))}var d,u,A,P,j,J,H,Y=e(&#39;&lt;div class=&quot;xdsoft_datetimepicker xdsoft_noselect&quot;&gt;&lt;/div&gt;&#39;),z=e(&#39;&lt;div class=&quot;xdsoft_copyright&quot;&gt;&lt;a target=&quot;_blank&quot; href=&quot;http://xdsoft.net/jqplugins/datetimepicker/&quot;&gt;xdsoft.net&lt;/a&gt;&lt;/div&gt;&#39;),N=e(&#39;&lt;div class=&quot;xdsoft_datepicker active&quot;&gt;&lt;/div&gt;&#39;),I=e(&#39;&lt;div class=&quot;xdsoft_mounthpicker&quot;&gt;&lt;button type=&quot;button&quot; class=&quot;xdsoft_prev&quot;&gt;&lt;/button&gt;&lt;button type=&quot;button&quot; class=&quot;xdsoft_today_button&quot;&gt;&lt;/button&gt;&lt;div class=&quot;xdsoft_label xdsoft_month&quot;&gt;&lt;span&gt;&lt;/span&gt;&lt;i&gt;&lt;/i&gt;&lt;/div&gt;&lt;div class=&quot;xdsoft_label xdsoft_year&quot;&gt;&lt;span&gt;&lt;/span&gt;&lt;i&gt;&lt;/i&gt;&lt;/div&gt;&lt;button type=&quot;button&quot; class=&quot;xdsoft_next&quot;&gt;&lt;/button&gt;&lt;/div&gt;&#39;),L=e(&#39;&lt;div class=&quot;xdsoft_calendar&quot;&gt;&lt;/div&gt;&#39;),R=e(&#39;&lt;div class=&quot;xdsoft_timepicker active&quot;&gt;&lt;button type=&quot;button&quot; class=&quot;xdsoft_prev&quot;&gt;&lt;/button&gt;&lt;div class=&quot;xdsoft_time_box&quot;&gt;&lt;/div&gt;&lt;button type=&quot;button&quot; class=&quot;xdsoft_next&quot;&gt;&lt;/button&gt;&lt;/div&gt;&#39;),V=R.find(&quot;.xdsoft_time_box&quot;).eq(0),E=e(&#39;&lt;div class=&quot;xdsoft_time_variant&quot;&gt;&lt;/div&gt;&#39;),B=e(&#39;&lt;button type=&quot;button&quot; class=&quot;xdsoft_save_selected blue-gradient-button&quot;&gt;Save Selected&lt;/button&gt;&#39;),G=e(&#39;&lt;div class=&quot;xdsoft_select xdsoft_monthselect&quot;&gt;&lt;div&gt;&lt;/div&gt;&lt;/div&gt;&#39;),q=e(&#39;&lt;div class=&quot;xdsoft_select xdsoft_yearselect&quot;&gt;&lt;div&gt;&lt;/div&gt;&lt;/div&gt;&#39;),K=!1,U=0;C.id&amp;&amp;Y.attr(&quot;id&quot;,C.id),C.style&amp;&amp;Y.attr(&quot;style&quot;,C.style),C.weeks&amp;&amp;Y.addClass(&quot;xdsoft_showweeks&quot;),C.rtl&amp;&amp;Y.addClass(&quot;xdsoft_rtl&quot;),Y.addClass(&quot;xdsoft_&quot;+C.theme),Y.addClass(C.className),I.find(&quot;.xdsoft_month span&quot;).after(G),I.find(&quot;.xdsoft_year span&quot;).after(q),I.find(&quot;.xdsoft_month,.xdsoft_year&quot;).on(&quot;touchstart mousedown.xdsoft&quot;,function(t){var a,o,r=e(this).find(&quot;.xdsoft_select&quot;).eq(0),n=0,i=0,s=r.is(&quot;:visible&quot;);for(I.find(&quot;.xdsoft_select&quot;).hide(),J.currentTime&amp;&amp;(n=J.currentTime[e(this).hasClass(&quot;xdsoft_month&quot;)?&quot;getMonth&quot;:&quot;getFullYear&quot;]()),r[s?&quot;hide&quot;:&quot;show&quot;](),a=r.find(&quot;div.xdsoft_option&quot;),o=0;o&lt;a.length&amp;&amp;a.eq(o).data(&quot;value&quot;)!==n;o+=1)i+=a[0].offsetHeight;return r.xdsoftScroller(i/(r.children()[0].offsetHeight-r[0].clientHeight)),t.stopPropagation(),!1}),I.find(&quot;.xdsoft_select&quot;).xdsoftScroller().on(&quot;touchstart mousedown.xdsoft&quot;,function(e){e.stopPropagation(),e.preventDefault()}).on(&quot;touchstart mousedown.xdsoft&quot;,&quot;.xdsoft_option&quot;,function(){(void 0===J.currentTime||null===J.currentTime)&amp;&amp;(J.currentTime=J.now());var t=J.currentTime.getFullYear();J&amp;&amp;J.currentTime&amp;&amp;J.currentTime[e(this).parent().parent().hasClass(&quot;xdsoft_monthselect&quot;)?&quot;setMonth&quot;:&quot;setFullYear&quot;](e(this).data(&quot;value&quot;)),e(this).parent().parent().hide(),Y.trigger(&quot;xchange.xdsoft&quot;),C.onChangeMonth&amp;&amp;e.isFunction(C.onChangeMonth)&amp;&amp;C.onChangeMonth.call(Y,J.currentTime,Y.data(&quot;input&quot;)),t!==J.currentTime.getFullYear()&amp;&amp;e.isFunction(C.onChangeYear)&amp;&amp;C.onChangeYear.call(Y,J.currentTime,Y.data(&quot;input&quot;))}),Y.getValue=function(){return J.getCurrentTime()},Y.setOptions=function(r){var n={};C=e.extend(!0,{},C,r),r.allowTimes&amp;&amp;e.isArray(r.allowTimes)&amp;&amp;r.allowTimes.length&amp;&amp;(C.allowTimes=e.extend(!0,[],r.allowTimes)),r.weekends&amp;&amp;e.isArray(r.weekends)&amp;&amp;r.weekends.length&amp;&amp;(C.weekends=e.extend(!0,[],r.weekends)),r.allowDates&amp;&amp;e.isArray(r.allowDates)&amp;&amp;r.allowDates.length&amp;&amp;(C.allowDates=e.extend(!0,[],r.allowDates)),r.allowDateRe&amp;&amp;&quot;[object String]&quot;===Object.prototype.toString.call(r.allowDateRe)&amp;&amp;(C.allowDateRe=new RegExp(r.allowDateRe)),r.highlightedDates&amp;&amp;e.isArray(r.highlightedDates)&amp;&amp;r.highlightedDates.length&amp;&amp;(e.each(r.highlightedDates,function(a,r){var i,s=e.map(r.split(&quot;,&quot;),e.trim),d=new t(o.parseDate(s[0],C.formatDate),s[1],s[2]),u=o.formatDate(d.date,C.formatDate);void 0!==n[u]?(i=n[u].desc,i&amp;&amp;i.length&amp;&amp;d.desc&amp;&amp;d.desc.length&amp;&amp;(n[u].desc=i+&quot;\n&quot;+d.desc)):n[u]=d}),C.highlightedDates=e.extend(!0,[],n)),r.highlightedPeriods&amp;&amp;e.isArray(r.highlightedPeriods)&amp;&amp;r.highlightedPeriods.length&amp;&amp;(n=e.extend(!0,[],C.highlightedDates),e.each(r.highlightedPeriods,function(a,r){var i,s,d,u,l,f,c;if(e.isArray(r))i=r[0],s=r[1],d=r[2],c=r[3];else{var m=e.map(r.split(&quot;,&quot;),e.trim);i=o.parseDate(m[0],C.formatDate),s=o.parseDate(m[1],C.formatDate),d=m[2],c=m[3]}for(;s&gt;=i;)u=new t(i,d,c),l=o.formatDate(i,C.formatDate),i.setDate(i.getDate()+1),void 0!==n[l]?(f=n[l].desc,f&amp;&amp;f.length&amp;&amp;u.desc&amp;&amp;u.desc.length&amp;&amp;(n[l].desc=f+&quot;\n&quot;+u.desc)):n[l]=u}),C.highlightedDates=e.extend(!0,[],n)),r.disabledDates&amp;&amp;e.isArray(r.disabledDates)&amp;&amp;r.disabledDates.length&amp;&amp;(C.disabledDates=e.extend(!0,[],r.disabledDates)),r.disabledWeekDays&amp;&amp;e.isArray(r.disabledWeekDays)&amp;&amp;r.disabledWeekDays.length&amp;&amp;(C.disabledWeekDays=e.extend(!0,[],r.disabledWeekDays)),!C.open&amp;&amp;!C.opened||C.inline||a.trigger(&quot;open.xdsoft&quot;),C.inline&amp;&amp;(K=!0,Y.addClass(&quot;xdsoft_inline&quot;),a.after(Y).hide()),C.inverseButton&amp;&amp;(C.next=&quot;xdsoft_prev&quot;,C.prev=&quot;xdsoft_next&quot;),C.datepicker?N.addClass(&quot;active&quot;):N.removeClass(&quot;active&quot;),C.timepicker?R.addClass(&quot;active&quot;):R.removeClass(&quot;active&quot;),C.value&amp;&amp;(J.setCurrentTime(C.value),a&amp;&amp;a.val&amp;&amp;a.val(J.str)),C.dayOfWeekStart=isNaN(C.dayOfWeekStart)?0:parseInt(C.dayOfWeekStart,10)%7,C.timepickerScrollbar||V.xdsoftScroller(&quot;hide&quot;),C.minDate&amp;&amp;/^[\+\-](.*)$/.test(C.minDate)&amp;&amp;(C.minDate=o.formatDate(J.strToDateTime(C.minDate),C.formatDate)),C.maxDate&amp;&amp;/^[\+\-](.*)$/.test(C.maxDate)&amp;&amp;(C.maxDate=o.formatDate(J.strToDateTime(C.maxDate),C.formatDate)),B.toggle(C.showApplyButton),I.find(&quot;.xdsoft_today_button&quot;).css(&quot;visibility&quot;,C.todayButton?&quot;visible&quot;:&quot;hidden&quot;),I.find(&quot;.&quot;+C.prev).css(&quot;visibility&quot;,C.prevButton?&quot;visible&quot;:&quot;hidden&quot;),I.find(&quot;.&quot;+C.next).css(&quot;visibility&quot;,C.nextButton?&quot;visible&quot;:&quot;hidden&quot;),s(C),C.validateOnBlur&amp;&amp;a.off(&quot;blur.xdsoft&quot;).on(&quot;blur.xdsoft&quot;,function(){if(C.allowBlank&amp;&amp;(!e.trim(e(this).val()).length||&quot;string&quot;==typeof C.mask&amp;&amp;e.trim(e(this).val())===C.mask.replace(/[0-9]/g,&quot;_&quot;)))e(this).val(null),Y.data(&quot;xdsoft_datetime&quot;).empty();else{var t=o.parseDate(e(this).val(),C.format);if(t)e(this).val(o.formatDate(t,C.format));else{var a=+[e(this).val()[0],e(this).val()[1]].join(&quot;&quot;),r=+[e(this).val()[2],e(this).val()[3]].join(&quot;&quot;);e(this).val(!C.datepicker&amp;&amp;C.timepicker&amp;&amp;a&gt;=0&amp;&amp;24&gt;a&amp;&amp;r&gt;=0&amp;&amp;60&gt;r?[a,r].map(function(e){return e&gt;9?e:&quot;0&quot;+e}).join(&quot;:&quot;):o.formatDate(J.now(),C.format))}Y.data(&quot;xdsoft_datetime&quot;).setCurrentTime(e(this).val())}Y.trigger(&quot;changedatetime.xdsoft&quot;),Y.trigger(&quot;close.xdsoft&quot;)}),C.dayOfWeekStartPrev=0===C.dayOfWeekStart?6:C.dayOfWeekStart-1,Y.trigger(&quot;xchange.xdsoft&quot;).trigger(&quot;afterOpen.xdsoft&quot;)},Y.data(&quot;options&quot;,C).on(&quot;touchstart mousedown.xdsoft&quot;,function(e){return e.stopPropagation(),e.preventDefault(),q.hide(),G.hide(),!1}),V.append(E),V.xdsoftScroller(),Y.on(&quot;afterOpen.xdsoft&quot;,function(){V.xdsoftScroller()}),Y.append(N).append(R),C.withoutCopyright!==!0&amp;&amp;Y.append(z),N.append(I).append(L).append(B),e(C.parentID).append(Y),d=function(){var t=this;t.now=function(e){var a,o,r=new Date;return!e&amp;&amp;C.defaultDate&amp;&amp;(a=t.strToDateTime(C.defaultDate),r.setFullYear(a.getFullYear()),r.setMonth(a.getMonth()),r.setDate(a.getDate())),C.yearOffset&amp;&amp;r.setFullYear(r.getFullYear()+C.yearOffset),!e&amp;&amp;C.defaultTime&amp;&amp;(o=t.strtotime(C.defaultTime),r.setHours(o.getHours()),r.setMinutes(o.getMinutes())),r},t.isValidDate=function(e){return&quot;[object Date]&quot;!==Object.prototype.toString.call(e)?!1:!isNaN(e.getTime())},t.setCurrentTime=function(e){t.currentTime=&quot;string&quot;==typeof e?t.strToDateTime(e):t.isValidDate(e)?e:t.now(),Y.trigger(&quot;xchange.xdsoft&quot;)},t.empty=function(){t.currentTime=null},t.getCurrentTime=function(){return t.currentTime},t.nextMonth=function(){(void 0===t.currentTime||null===t.currentTime)&amp;&amp;(t.currentTime=t.now());var a,o=t.currentTime.getMonth()+1;return 12===o&amp;&amp;(t.currentTime.setFullYear(t.currentTime.getFullYear()+1),o=0),a=t.currentTime.getFullYear(),t.currentTime.setDate(Math.min(new Date(t.currentTime.getFullYear(),o+1,0).getDate(),t.currentTime.getDate())),t.currentTime.setMonth(o),C.onChangeMonth&amp;&amp;e.isFunction(C.onChangeMonth)&amp;&amp;C.onChangeMonth.call(Y,J.currentTime,Y.data(&quot;input&quot;)),a!==t.currentTime.getFullYear()&amp;&amp;e.isFunction(C.onChangeYear)&amp;&amp;C.onChangeYear.call(Y,J.currentTime,Y.data(&quot;input&quot;)),Y.trigger(&quot;xchange.xdsoft&quot;),o},t.prevMonth=function(){(void 0===t.currentTime||null===t.currentTime)&amp;&amp;(t.currentTime=t.now());var a=t.currentTime.getMonth()-1;return-1===a&amp;&amp;(t.currentTime.setFullYear(t.currentTime.getFullYear()-1),a=11),t.currentTime.setDate(Math.min(new Date(t.currentTime.getFullYear(),a+1,0).getDate(),t.currentTime.getDate())),t.currentTime.setMonth(a),C.onChangeMonth&amp;&amp;e.isFunction(C.onChangeMonth)&amp;&amp;C.onChangeMonth.call(Y,J.currentTime,Y.data(&quot;input&quot;)),Y.trigger(&quot;xchange.xdsoft&quot;),a},t.getWeekOfYear=function(t){if(C.onGetWeekOfYear&amp;&amp;e.isFunction(C.onGetWeekOfYear)){var a=C.onGetWeekOfYear.call(Y,t);if(&quot;undefined&quot;!=typeof a)return a}var o=new Date(t.getFullYear(),0,1);return 4!=o.getDay()&amp;&amp;o.setMonth(0,1+(4-o.getDay()+7)%7),Math.ceil(((t-o)/864e5+o.getDay()+1)/7)},t.strToDateTime=function(e){var a,r,n=[];return e&amp;&amp;e instanceof Date&amp;&amp;t.isValidDate(e)?e:(n=/^(\+|\-)(.*)$/.exec(e),n&amp;&amp;(n[2]=o.parseDate(n[2],C.formatDate)),n&amp;&amp;n[2]?(a=n[2].getTime()-6e4*n[2].getTimezoneOffset(),r=new Date(t.now(!0).getTime()+parseInt(n[1]+&quot;1&quot;,10)*a)):r=e?o.parseDate(e,C.format):t.now(),t.isValidDate(r)||(r=t.now()),r)},t.strToDate=function(e){if(e&amp;&amp;e instanceof Date&amp;&amp;t.isValidDate(e))return e;var a=e?o.parseDate(e,C.formatDate):t.now(!0);return t.isValidDate(a)||(a=t.now(!0)),a},t.strtotime=function(e){if(e&amp;&amp;e instanceof Date&amp;&amp;t.isValidDate(e))return e;var a=e?o.parseDate(e,C.formatTime):t.now(!0);return t.isValidDate(a)||(a=t.now(!0)),a},t.str=function(){return o.formatDate(t.currentTime,C.format)},t.currentTime=this.now()},J=new d,B.on(&quot;touchend click&quot;,function(e){e.preventDefault(),Y.data(&quot;changed&quot;,!0),J.setCurrentTime(i()),a.val(J.str()),Y.trigger(&quot;close.xdsoft&quot;)}),I.find(&quot;.xdsoft_today_button&quot;).on(&quot;touchend mousedown.xdsoft&quot;,function(){</td>
      </tr>
      <tr>
        <td id="L2" class="blob-num js-line-number" data-line-number="2"></td>
        <td id="LC2" class="blob-code blob-code-inner js-file-line">Y.data(&quot;changed&quot;,!0),J.setCurrentTime(0),Y.trigger(&quot;afterOpen.xdsoft&quot;)}).on(&quot;dblclick.xdsoft&quot;,function(){var e,t,o=J.getCurrentTime();o=new Date(o.getFullYear(),o.getMonth(),o.getDate()),e=J.strToDate(C.minDate),e=new Date(e.getFullYear(),e.getMonth(),e.getDate()),e&gt;o||(t=J.strToDate(C.maxDate),t=new Date(t.getFullYear(),t.getMonth(),t.getDate()),o&gt;t||(a.val(J.str()),a.trigger(&quot;change&quot;),Y.trigger(&quot;close.xdsoft&quot;)))}),I.find(&quot;.xdsoft_prev,.xdsoft_next&quot;).on(&quot;touchend mousedown.xdsoft&quot;,function(){var t=e(this),a=0,o=!1;!function r(e){t.hasClass(C.next)?J.nextMonth():t.hasClass(C.prev)&amp;&amp;J.prevMonth(),C.monthChangeSpinner&amp;&amp;(o||(a=setTimeout(r,e||100)))}(500),e([document.body,window]).on(&quot;touchend mouseup.xdsoft&quot;,function n(){clearTimeout(a),o=!0,e([document.body,window]).off(&quot;touchend mouseup.xdsoft&quot;,n)})}),R.find(&quot;.xdsoft_prev,.xdsoft_next&quot;).on(&quot;touchend mousedown.xdsoft&quot;,function(){var t=e(this),a=0,o=!1,r=110;!function n(e){var i=V[0].clientHeight,s=E[0].offsetHeight,d=Math.abs(parseInt(E.css(&quot;marginTop&quot;),10));t.hasClass(C.next)&amp;&amp;s-i-C.timeHeightInTimePicker&gt;=d?E.css(&quot;marginTop&quot;,&quot;-&quot;+(d+C.timeHeightInTimePicker)+&quot;px&quot;):t.hasClass(C.prev)&amp;&amp;d-C.timeHeightInTimePicker&gt;=0&amp;&amp;E.css(&quot;marginTop&quot;,&quot;-&quot;+(d-C.timeHeightInTimePicker)+&quot;px&quot;),V.trigger(&quot;scroll_element.xdsoft_scroller&quot;,[Math.abs(parseInt(E[0].style.marginTop,10)/(s-i))]),r=r&gt;10?10:r-10,o||(a=setTimeout(n,e||r))}(500),e([document.body,window]).on(&quot;touchend mouseup.xdsoft&quot;,function i(){clearTimeout(a),o=!0,e([document.body,window]).off(&quot;touchend mouseup.xdsoft&quot;,i)})}),u=0,Y.on(&quot;xchange.xdsoft&quot;,function(t){clearTimeout(u),u=setTimeout(function(){(void 0===J.currentTime||null===J.currentTime)&amp;&amp;(J.currentTime=J.now());for(var t,i,s,d,u,l,f,c,m,h,g=&quot;&quot;,p=new Date(J.currentTime.getFullYear(),J.currentTime.getMonth(),1,12,0,0),y=0,k=J.now(),b=!1,x=!1,v=[],D=!0,T=&quot;&quot;,S=&quot;&quot;;p.getDay()!==C.dayOfWeekStart;)p.setDate(p.getDate()-1);for(g+=&quot;&lt;table&gt;&lt;thead&gt;&lt;tr&gt;&quot;,C.weeks&amp;&amp;(g+=&quot;&lt;th&gt;&lt;/th&gt;&quot;),t=0;7&gt;t;t+=1)g+=&quot;&lt;th&gt;&quot;+C.i18n[n].dayOfWeekShort[(t+C.dayOfWeekStart)%7]+&quot;&lt;/th&gt;&quot;;for(g+=&quot;&lt;/tr&gt;&lt;/thead&gt;&quot;,g+=&quot;&lt;tbody&gt;&quot;,C.maxDate!==!1&amp;&amp;(b=J.strToDate(C.maxDate),b=new Date(b.getFullYear(),b.getMonth(),b.getDate(),23,59,59,999)),C.minDate!==!1&amp;&amp;(x=J.strToDate(C.minDate),x=new Date(x.getFullYear(),x.getMonth(),x.getDate()));y&lt;J.currentTime.countDaysInMonth()||p.getDay()!==C.dayOfWeekStart||J.currentTime.getMonth()===p.getMonth();)v=[],y+=1,s=p.getDay(),d=p.getDate(),u=p.getFullYear(),l=p.getMonth(),f=J.getWeekOfYear(p),h=&quot;&quot;,v.push(&quot;xdsoft_date&quot;),c=C.beforeShowDay&amp;&amp;e.isFunction(C.beforeShowDay.call)?C.beforeShowDay.call(Y,p):null,C.allowDateRe&amp;&amp;&quot;[object RegExp]&quot;===Object.prototype.toString.call(C.allowDateRe)?C.allowDateRe.test(o.formatDate(p,C.formatDate))||v.push(&quot;xdsoft_disabled&quot;):C.allowDates&amp;&amp;C.allowDates.length&gt;0?-1===C.allowDates.indexOf(o.formatDate(p,C.formatDate))&amp;&amp;v.push(&quot;xdsoft_disabled&quot;):b!==!1&amp;&amp;p&gt;b||x!==!1&amp;&amp;x&gt;p||c&amp;&amp;c[0]===!1?v.push(&quot;xdsoft_disabled&quot;):-1!==C.disabledDates.indexOf(o.formatDate(p,C.formatDate))?v.push(&quot;xdsoft_disabled&quot;):-1!==C.disabledWeekDays.indexOf(s)?v.push(&quot;xdsoft_disabled&quot;):a.is(&quot;[readonly]&quot;)&amp;&amp;v.push(&quot;xdsoft_disabled&quot;),c&amp;&amp;&quot;&quot;!==c[1]&amp;&amp;v.push(c[1]),J.currentTime.getMonth()!==l&amp;&amp;v.push(&quot;xdsoft_other_month&quot;),(C.defaultSelect||Y.data(&quot;changed&quot;))&amp;&amp;o.formatDate(J.currentTime,C.formatDate)===o.formatDate(p,C.formatDate)&amp;&amp;v.push(&quot;xdsoft_current&quot;),o.formatDate(k,C.formatDate)===o.formatDate(p,C.formatDate)&amp;&amp;v.push(&quot;xdsoft_today&quot;),(0===p.getDay()||6===p.getDay()||-1!==C.weekends.indexOf(o.formatDate(p,C.formatDate)))&amp;&amp;v.push(&quot;xdsoft_weekend&quot;),void 0!==C.highlightedDates[o.formatDate(p,C.formatDate)]&amp;&amp;(i=C.highlightedDates[o.formatDate(p,C.formatDate)],v.push(void 0===i.style?&quot;xdsoft_highlighted_default&quot;:i.style),h=void 0===i.desc?&quot;&quot;:i.desc),C.beforeShowDay&amp;&amp;e.isFunction(C.beforeShowDay)&amp;&amp;v.push(C.beforeShowDay(p)),D&amp;&amp;(g+=&quot;&lt;tr&gt;&quot;,D=!1,C.weeks&amp;&amp;(g+=&quot;&lt;th&gt;&quot;+f+&quot;&lt;/th&gt;&quot;)),g+=&#39;&lt;td data-date=&quot;&#39;+d+&#39;&quot; data-month=&quot;&#39;+l+&#39;&quot; data-year=&quot;&#39;+u+&#39;&quot; class=&quot;xdsoft_date xdsoft_day_of_week&#39;+p.getDay()+&quot; &quot;+v.join(&quot; &quot;)+&#39;&quot; title=&quot;&#39;+h+&#39;&quot;&gt;&lt;div&gt;&#39;+d+&quot;&lt;/div&gt;&lt;/td&gt;&quot;,p.getDay()===C.dayOfWeekStartPrev&amp;&amp;(g+=&quot;&lt;/tr&gt;&quot;,D=!0),p.setDate(d+1);if(g+=&quot;&lt;/tbody&gt;&lt;/table&gt;&quot;,L.html(g),I.find(&quot;.xdsoft_label span&quot;).eq(0).text(C.i18n[n].months[J.currentTime.getMonth()]),I.find(&quot;.xdsoft_label span&quot;).eq(1).text(J.currentTime.getFullYear()),T=&quot;&quot;,S=&quot;&quot;,l=&quot;&quot;,m=function(t,r){var n,i,s=J.now(),d=C.allowTimes&amp;&amp;e.isArray(C.allowTimes)&amp;&amp;C.allowTimes.length;s.setHours(t),t=parseInt(s.getHours(),10),s.setMinutes(r),r=parseInt(s.getMinutes(),10),n=new Date(J.currentTime),n.setHours(t),n.setMinutes(r),v=[],C.minDateTime!==!1&amp;&amp;C.minDateTime&gt;n||C.maxTime!==!1&amp;&amp;J.strtotime(C.maxTime).getTime()&lt;s.getTime()||C.minTime!==!1&amp;&amp;J.strtotime(C.minTime).getTime()&gt;s.getTime()?v.push(&quot;xdsoft_disabled&quot;):C.minDateTime!==!1&amp;&amp;C.minDateTime&gt;n||C.disabledMinTime!==!1&amp;&amp;s.getTime()&gt;J.strtotime(C.disabledMinTime).getTime()&amp;&amp;C.disabledMaxTime!==!1&amp;&amp;s.getTime()&lt;J.strtotime(C.disabledMaxTime).getTime()?v.push(&quot;xdsoft_disabled&quot;):a.is(&quot;[readonly]&quot;)&amp;&amp;v.push(&quot;xdsoft_disabled&quot;),i=new Date(J.currentTime),i.setHours(parseInt(J.currentTime.getHours(),10)),d||i.setMinutes(Math[C.roundTime](J.currentTime.getMinutes()/C.step)*C.step),(C.initTime||C.defaultSelect||Y.data(&quot;changed&quot;))&amp;&amp;i.getHours()===parseInt(t,10)&amp;&amp;(!d&amp;&amp;C.step&gt;59||i.getMinutes()===parseInt(r,10))&amp;&amp;(C.defaultSelect||Y.data(&quot;changed&quot;)?v.push(&quot;xdsoft_current&quot;):C.initTime&amp;&amp;v.push(&quot;xdsoft_init_time&quot;)),parseInt(k.getHours(),10)===parseInt(t,10)&amp;&amp;parseInt(k.getMinutes(),10)===parseInt(r,10)&amp;&amp;v.push(&quot;xdsoft_today&quot;),T+=&#39;&lt;div class=&quot;xdsoft_time &#39;+v.join(&quot; &quot;)+&#39;&quot; data-hour=&quot;&#39;+t+&#39;&quot; data-minute=&quot;&#39;+r+&#39;&quot;&gt;&#39;+o.formatDate(s,C.formatTime)+&quot;&lt;/div&gt;&quot;},C.allowTimes&amp;&amp;e.isArray(C.allowTimes)&amp;&amp;C.allowTimes.length)for(y=0;y&lt;C.allowTimes.length;y+=1)S=J.strtotime(C.allowTimes[y]).getHours(),l=J.strtotime(C.allowTimes[y]).getMinutes(),m(S,l);else for(y=0,t=0;y&lt;(C.hours12?12:24);y+=1)for(t=0;60&gt;t;t+=C.step)S=(10&gt;y?&quot;0&quot;:&quot;&quot;)+y,l=(10&gt;t?&quot;0&quot;:&quot;&quot;)+t,m(S,l);for(E.html(T),r=&quot;&quot;,y=0,y=parseInt(C.yearStart,10)+C.yearOffset;y&lt;=parseInt(C.yearEnd,10)+C.yearOffset;y+=1)r+=&#39;&lt;div class=&quot;xdsoft_option &#39;+(J.currentTime.getFullYear()===y?&quot;xdsoft_current&quot;:&quot;&quot;)+&#39;&quot; data-value=&quot;&#39;+y+&#39;&quot;&gt;&#39;+y+&quot;&lt;/div&gt;&quot;;for(q.children().eq(0).html(r),y=parseInt(C.monthStart,10),r=&quot;&quot;;y&lt;=parseInt(C.monthEnd,10);y+=1)r+=&#39;&lt;div class=&quot;xdsoft_option &#39;+(J.currentTime.getMonth()===y?&quot;xdsoft_current&quot;:&quot;&quot;)+&#39;&quot; data-value=&quot;&#39;+y+&#39;&quot;&gt;&#39;+C.i18n[n].months[y]+&quot;&lt;/div&gt;&quot;;G.children().eq(0).html(r),e(Y).trigger(&quot;generate.xdsoft&quot;)},10),t.stopPropagation()}).on(&quot;afterOpen.xdsoft&quot;,function(){if(C.timepicker){var e,t,a,o;E.find(&quot;.xdsoft_current&quot;).length?e=&quot;.xdsoft_current&quot;:E.find(&quot;.xdsoft_init_time&quot;).length&amp;&amp;(e=&quot;.xdsoft_init_time&quot;),e?(t=V[0].clientHeight,a=E[0].offsetHeight,o=E.find(e).index()*C.timeHeightInTimePicker+1,o&gt;a-t&amp;&amp;(o=a-t),V.trigger(&quot;scroll_element.xdsoft_scroller&quot;,[parseInt(o,10)/(a-t)])):V.trigger(&quot;scroll_element.xdsoft_scroller&quot;,[0])}}),A=0,L.on(&quot;touchend click.xdsoft&quot;,&quot;td&quot;,function(t){t.stopPropagation(),A+=1;var o=e(this),r=J.currentTime;return(void 0===r||null===r)&amp;&amp;(J.currentTime=J.now(),r=J.currentTime),o.hasClass(&quot;xdsoft_disabled&quot;)?!1:(r.setDate(1),r.setFullYear(o.data(&quot;year&quot;)),r.setMonth(o.data(&quot;month&quot;)),r.setDate(o.data(&quot;date&quot;)),Y.trigger(&quot;select.xdsoft&quot;,[r]),a.val(J.str()),C.onSelectDate&amp;&amp;e.isFunction(C.onSelectDate)&amp;&amp;C.onSelectDate.call(Y,J.currentTime,Y.data(&quot;input&quot;),t),Y.data(&quot;changed&quot;,!0),Y.trigger(&quot;xchange.xdsoft&quot;),Y.trigger(&quot;changedatetime.xdsoft&quot;),(A&gt;1||C.closeOnDateSelect===!0||C.closeOnDateSelect===!1&amp;&amp;!C.timepicker)&amp;&amp;!C.inline&amp;&amp;Y.trigger(&quot;close.xdsoft&quot;),void setTimeout(function(){A=0},200))}),E.on(&quot;touchend click.xdsoft&quot;,&quot;div&quot;,function(t){t.stopPropagation();var a=e(this),o=J.currentTime;return(void 0===o||null===o)&amp;&amp;(J.currentTime=J.now(),o=J.currentTime),a.hasClass(&quot;xdsoft_disabled&quot;)?!1:(o.setHours(a.data(&quot;hour&quot;)),o.setMinutes(a.data(&quot;minute&quot;)),Y.trigger(&quot;select.xdsoft&quot;,[o]),Y.data(&quot;input&quot;).val(J.str()),C.onSelectTime&amp;&amp;e.isFunction(C.onSelectTime)&amp;&amp;C.onSelectTime.call(Y,J.currentTime,Y.data(&quot;input&quot;),t),Y.data(&quot;changed&quot;,!0),Y.trigger(&quot;xchange.xdsoft&quot;),Y.trigger(&quot;changedatetime.xdsoft&quot;),void(C.inline!==!0&amp;&amp;C.closeOnTimeSelect===!0&amp;&amp;Y.trigger(&quot;close.xdsoft&quot;)))}),N.on(&quot;mousewheel.xdsoft&quot;,function(e){return C.scrollMonth?(e.deltaY&lt;0?J.nextMonth():J.prevMonth(),!1):!0}),a.on(&quot;mousewheel.xdsoft&quot;,function(e){return C.scrollInput?!C.datepicker&amp;&amp;C.timepicker?(P=E.find(&quot;.xdsoft_current&quot;).length?E.find(&quot;.xdsoft_current&quot;).eq(0).index():0,P+e.deltaY&gt;=0&amp;&amp;P+e.deltaY&lt;E.children().length&amp;&amp;(P+=e.deltaY),E.children().eq(P).length&amp;&amp;E.children().eq(P).trigger(&quot;mousedown&quot;),!1):C.datepicker&amp;&amp;!C.timepicker?(N.trigger(e,[e.deltaY,e.deltaX,e.deltaY]),a.val&amp;&amp;a.val(J.str()),Y.trigger(&quot;changedatetime.xdsoft&quot;),!1):void 0:!0}),Y.on(&quot;changedatetime.xdsoft&quot;,function(t){if(C.onChangeDateTime&amp;&amp;e.isFunction(C.onChangeDateTime)){var a=Y.data(&quot;input&quot;);C.onChangeDateTime.call(Y,J.currentTime,a,t),delete C.value,a.trigger(&quot;change&quot;)}}).on(&quot;generate.xdsoft&quot;,function(){C.onGenerate&amp;&amp;e.isFunction(C.onGenerate)&amp;&amp;C.onGenerate.call(Y,J.currentTime,Y.data(&quot;input&quot;)),K&amp;&amp;(Y.trigger(&quot;afterOpen.xdsoft&quot;),K=!1)}).on(&quot;click.xdsoft&quot;,function(e){e.stopPropagation()}),P=0,H=function(e,t){do if(e=e.parentNode,t(e)===!1)break;while(&quot;HTML&quot;!==e.nodeName)},j=function(){var t,a,o,r,n,i,s,d,u,l,f,c,m;if(d=Y.data(&quot;input&quot;),t=d.offset(),a=d[0],l=&quot;top&quot;,o=t.top+a.offsetHeight-1,r=t.left,n=&quot;absolute&quot;,u=e(window).width(),c=e(window).height(),m=e(window).scrollTop(),document.documentElement.clientWidth-t.left&lt;N.parent().outerWidth(!0)){var h=N.parent().outerWidth(!0)-a.offsetWidth;r-=h}&quot;rtl&quot;===d.parent().css(&quot;direction&quot;)&amp;&amp;(r-=Y.outerWidth()-d.outerWidth()),C.fixed?(o-=m,r-=e(window).scrollLeft(),n=&quot;fixed&quot;):(s=!1,H(a,function(e){return&quot;fixed&quot;===window.getComputedStyle(e).getPropertyValue(&quot;position&quot;)?(s=!0,!1):void 0}),s?(n=&quot;fixed&quot;,o+Y.outerHeight()&gt;c+m?(l=&quot;bottom&quot;,o=c+m-t.top):o-=m):o+a.offsetHeight&gt;c+m&amp;&amp;(o=t.top-a.offsetHeight+1),0&gt;o&amp;&amp;(o=0),r+a.offsetWidth&gt;u&amp;&amp;(r=u-a.offsetWidth)),i=Y[0],H(i,function(e){var t;return t=window.getComputedStyle(e).getPropertyValue(&quot;position&quot;),&quot;relative&quot;===t&amp;&amp;u&gt;=e.offsetWidth?(r-=(u-e.offsetWidth)/2,!1):void 0}),f={position:n,left:r,top:&quot;&quot;,bottom:&quot;&quot;},f[l]=o,Y.css(f)},Y.on(&quot;open.xdsoft&quot;,function(t){var a=!0;C.onShow&amp;&amp;e.isFunction(C.onShow)&amp;&amp;(a=C.onShow.call(Y,J.currentTime,Y.data(&quot;input&quot;),t)),a!==!1&amp;&amp;(Y.show(),j(),e(window).off(&quot;resize.xdsoft&quot;,j).on(&quot;resize.xdsoft&quot;,j),C.closeOnWithoutClick&amp;&amp;e([document.body,window]).on(&quot;touchstart mousedown.xdsoft&quot;,function o(){Y.trigger(&quot;close.xdsoft&quot;),e([document.body,window]).off(&quot;touchstart mousedown.xdsoft&quot;,o)}))}).on(&quot;close.xdsoft&quot;,function(t){var a=!0;I.find(&quot;.xdsoft_month,.xdsoft_year&quot;).find(&quot;.xdsoft_select&quot;).hide(),C.onClose&amp;&amp;e.isFunction(C.onClose)&amp;&amp;(a=C.onClose.call(Y,J.currentTime,Y.data(&quot;input&quot;),t)),a===!1||C.opened||C.inline||Y.hide(),t.stopPropagation()}).on(&quot;toggle.xdsoft&quot;,function(){Y.trigger(Y.is(&quot;:visible&quot;)?&quot;close.xdsoft&quot;:&quot;open.xdsoft&quot;)}).data(&quot;input&quot;,a),U=0,Y.data(&quot;xdsoft_datetime&quot;,J),Y.setOptions(C),J.setCurrentTime(i()),a.data(&quot;xdsoft_datetimepicker&quot;,Y).on(&quot;open.xdsoft focusin.xdsoft mousedown.xdsoft touchstart&quot;,function(){a.is(&quot;:disabled&quot;)||a.data(&quot;xdsoft_datetimepicker&quot;).is(&quot;:visible&quot;)&amp;&amp;C.closeOnInputClick||(clearTimeout(U),U=setTimeout(function(){a.is(&quot;:disabled&quot;)||(K=!0,J.setCurrentTime(i()),C.mask&amp;&amp;s(C),Y.trigger(&quot;open.xdsoft&quot;))},100))}).on(&quot;keydown.xdsoft&quot;,function(t){var a,o=t.which;return-1!==[p].indexOf(o)&amp;&amp;C.enterLikeTab?(a=e(&quot;input:visible,textarea:visible,button:visible,a:visible&quot;),Y.trigger(&quot;close.xdsoft&quot;),a.eq(a.index(this)+1).focus(),!1):-1!==[T].indexOf(o)?(Y.trigger(&quot;close.xdsoft&quot;),!0):void 0}).on(&quot;blur.xdsoft&quot;,function(){Y.trigger(&quot;close.xdsoft&quot;)})},d=function(t){var a=t.data(&quot;xdsoft_datetimepicker&quot;);a&amp;&amp;(a.data(&quot;xdsoft_datetime&quot;,null),a.remove(),t.data(&quot;xdsoft_datetimepicker&quot;,null).off(&quot;.xdsoft&quot;),e(window).off(&quot;resize.xdsoft&quot;),e([window,document.body]).off(&quot;mousedown.xdsoft touchstart&quot;),t.unmousewheel&amp;&amp;t.unmousewheel())},e(document).off(&quot;keydown.xdsoftctrl keyup.xdsoftctrl&quot;).on(&quot;keydown.xdsoftctrl&quot;,function(e){e.keyCode===h&amp;&amp;(F=!0)}).on(&quot;keyup.xdsoftctrl&quot;,function(e){e.keyCode===h&amp;&amp;(F=!1)}),this.each(function(){var t,a=e(this).data(&quot;xdsoft_datetimepicker&quot;);if(a){if(&quot;string&quot;===e.type(r))switch(r){case&quot;show&quot;:e(this).select().focus(),a.trigger(&quot;open.xdsoft&quot;);break;case&quot;hide&quot;:a.trigger(&quot;close.xdsoft&quot;);break;case&quot;toggle&quot;:a.trigger(&quot;toggle.xdsoft&quot;);break;case&quot;destroy&quot;:d(e(this));break;case&quot;reset&quot;:this.value=this.defaultValue,this.value&amp;&amp;a.data(&quot;xdsoft_datetime&quot;).isValidDate(o.parseDate(this.value,C.format))||a.data(&quot;changed&quot;,!1),a.data(&quot;xdsoft_datetime&quot;).setCurrentTime(this.value);break;case&quot;validate&quot;:t=a.data(&quot;input&quot;),t.trigger(&quot;blur.xdsoft&quot;);break;default:a[r]&amp;&amp;e.isFunction(a[r])&amp;&amp;(u=a[r](i))}else a.setOptions(r);return 0}&quot;string&quot;!==e.type(r)&amp;&amp;(!C.lazyInit||C.open||C.inline?s(e(this)):P(e(this)))}),u},e.fn.datetimepicker.defaults=a});</td>
      </tr>
</table>

  </div>

</div>

<button type="button" data-facebox="#jump-to-line" data-facebox-class="linejump" data-hotkey="l" class="hidden">Jump to Line</button>
<div id="jump-to-line" style="display:none">
  <!-- </textarea> --><!-- '"` --><form accept-charset="UTF-8" action="" class="js-jump-to-line-form" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /></div>
    <input class="form-control linejump-input js-jump-to-line-field" type="text" placeholder="Jump to line&hellip;" aria-label="Jump to line" autofocus>
    <button type="submit" class="btn">Go</button>
</form></div>

  </div>
  <div class="modal-backdrop"></div>
</div>


    </div>
  </div>

    </div>

        <div class="container site-footer-container">
  <div class="site-footer" role="contentinfo">
    <ul class="site-footer-links right">
        <li><a href="https://status.github.com/" data-ga-click="Footer, go to status, text:status">Status</a></li>
      <li><a href="https://developer.github.com" data-ga-click="Footer, go to api, text:api">API</a></li>
      <li><a href="https://training.github.com" data-ga-click="Footer, go to training, text:training">Training</a></li>
      <li><a href="https://shop.github.com" data-ga-click="Footer, go to shop, text:shop">Shop</a></li>
        <li><a href="https://github.com/blog" data-ga-click="Footer, go to blog, text:blog">Blog</a></li>
        <li><a href="https://github.com/about" data-ga-click="Footer, go to about, text:about">About</a></li>

    </ul>

    <a href="https://github.com" aria-label="Homepage" class="site-footer-mark" title="GitHub">
      <svg aria-hidden="true" class="octicon octicon-mark-github" height="24" version="1.1" viewBox="0 0 16 16" width="24"><path d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59 0.4 0.07 0.55-0.17 0.55-0.38 0-0.19-0.01-0.82-0.01-1.49-2.01 0.37-2.53-0.49-2.69-0.94-0.09-0.23-0.48-0.94-0.82-1.13-0.28-0.15-0.68-0.52-0.01-0.53 0.63-0.01 1.08 0.58 1.23 0.82 0.72 1.21 1.87 0.87 2.33 0.66 0.07-0.52 0.28-0.87 0.51-1.07-1.78-0.2-3.64-0.89-3.64-3.95 0-0.87 0.31-1.59 0.82-2.15-0.08-0.2-0.36-1.02 0.08-2.12 0 0 0.67-0.21 2.2 0.82 0.64-0.18 1.32-0.27 2-0.27 0.68 0 1.36 0.09 2 0.27 1.53-1.04 2.2-0.82 2.2-0.82 0.44 1.1 0.16 1.92 0.08 2.12 0.51 0.56 0.82 1.27 0.82 2.15 0 3.07-1.87 3.75-3.65 3.95 0.29 0.25 0.54 0.73 0.54 1.48 0 1.07-0.01 1.93-0.01 2.2 0 0.21 0.15 0.46 0.55 0.38C13.71 14.53 16 11.53 16 8 16 3.58 12.42 0 8 0z"></path></svg>
</a>
    <ul class="site-footer-links">
      <li>&copy; 2016 <span title="0.10370s from github-fe133-cp1-prd.iad.github.net">GitHub</span>, Inc.</li>
        <li><a href="https://github.com/site/terms" data-ga-click="Footer, go to terms, text:terms">Terms</a></li>
        <li><a href="https://github.com/site/privacy" data-ga-click="Footer, go to privacy, text:privacy">Privacy</a></li>
        <li><a href="https://github.com/security" data-ga-click="Footer, go to security, text:security">Security</a></li>
        <li><a href="https://github.com/contact" data-ga-click="Footer, go to contact, text:contact">Contact</a></li>
        <li><a href="https://help.github.com" data-ga-click="Footer, go to help, text:help">Help</a></li>
    </ul>
  </div>
</div>



    
    

    <div id="ajax-error-message" class="ajax-error-message flash flash-error">
      <svg aria-hidden="true" class="octicon octicon-alert" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M15.72 12.5l-6.85-11.98C8.69 0.21 8.36 0.02 8 0.02s-0.69 0.19-0.87 0.5l-6.85 11.98c-0.18 0.31-0.18 0.69 0 1C0.47 13.81 0.8 14 1.15 14h13.7c0.36 0 0.69-0.19 0.86-0.5S15.89 12.81 15.72 12.5zM9 12H7V10h2V12zM9 9H7V5h2V9z"></path></svg>
      <button type="button" class="flash-close js-flash-close js-ajax-error-dismiss" aria-label="Dismiss error">
        <svg aria-hidden="true" class="octicon octicon-x" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M7.48 8l3.75 3.75-1.48 1.48-3.75-3.75-3.75 3.75-1.48-1.48 3.75-3.75L0.77 4.25l1.48-1.48 3.75 3.75 3.75-3.75 1.48 1.48-3.75 3.75z"></path></svg>
      </button>
      Something went wrong with that request. Please try again.
    </div>


      
      <script crossorigin="anonymous" integrity="sha256-Y10ep2pJaZRKVENjXllmHxJznUueeidJ0YWiIyIHKHc=" src="https://assets-cdn.github.com/assets/frameworks-635d1ea76a4969944a5443635e59661f12739d4b9e7a2749d185a22322072877.js"></script>
      <script async="async" crossorigin="anonymous" integrity="sha256-FYf5Kg3pMiF1yiiOPog81kH3FyAqKh9HMmnsXaTGW5I=" src="https://assets-cdn.github.com/assets/github-1587f92a0de9322175ca288e3e883cd641f717202a2a1f473269ec5da4c65b92.js"></script>
      
      
      
      
      
      
    <div class="js-stale-session-flash stale-session-flash flash flash-warn flash-banner hidden">
      <svg aria-hidden="true" class="octicon octicon-alert" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path d="M15.72 12.5l-6.85-11.98C8.69 0.21 8.36 0.02 8 0.02s-0.69 0.19-0.87 0.5l-6.85 11.98c-0.18 0.31-0.18 0.69 0 1C0.47 13.81 0.8 14 1.15 14h13.7c0.36 0 0.69-0.19 0.86-0.5S15.89 12.81 15.72 12.5zM9 12H7V10h2V12zM9 9H7V5h2V9z"></path></svg>
      <span class="signed-in-tab-flash">You signed in with another tab or window. <a href="">Reload</a> to refresh your session.</span>
      <span class="signed-out-tab-flash">You signed out in another tab or window. <a href="">Reload</a> to refresh your session.</span>
    </div>
    <div class="facebox" id="facebox" style="display:none;">
  <div class="facebox-popup">
    <div class="facebox-content" role="dialog" aria-labelledby="facebox-header" aria-describedby="facebox-description">
    </div>
    <button type="button" class="facebox-close js-facebox-close" aria-label="Close modal">
      <svg aria-hidden="true" class="octicon octicon-x" height="16" version="1.1" viewBox="0 0 12 16" width="12"><path d="M7.48 8l3.75 3.75-1.48 1.48-3.75-3.75-3.75 3.75-1.48-1.48 3.75-3.75L0.77 4.25l1.48-1.48 3.75 3.75 3.75-3.75 1.48 1.48-3.75 3.75z"></path></svg>
    </button>
  </div>
</div>

  </body>
</html>

