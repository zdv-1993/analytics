import datetime
from django.conf import settings
from django.core.paginator import Paginator
from django.views.generic import TemplateView
from django.views.generic.edit import FormView
import os
import logging
import httplib2

from googleapiclient.discovery import build
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from django.shortcuts import render
# from django_sample.plus.models import CredentialsModel
from oauth2client.contrib import xsrfutil
from oauth2client.client import flow_from_clientsecrets
from oauth2client.contrib.django_orm import Storage

# CLIENT_SECRETS, name of a file containing the OAuth 2.0 information for this
# application, including client_id and client_secret, which are found
# on the API Access tab on the Google APIs
# Console <http://code.google.com/apis/console>
from apps.analytics_lib import SingletoneGoogleApi
from apps.forms import AddEventForm, AddSiteForm
from apps.models import CredentialsModel
from apps import analytics_lib

CLIENT_SECRETS = os.path.join(os.path.dirname(__file__), '../client_secret.json')

FLOW = flow_from_clientsecrets(
    CLIENT_SECRETS,
    scope=[
        # 'https://www.googleapis.com/auth/analytics.readonly',
        'https://www.googleapis.com/auth/analytics',
        'https://www.googleapis.com/auth/userinfo.profile',
        'https://www.googleapis.com/auth/analytics.edit',
        'https://www.googleapis.com/auth/analytics.manage.users'
    ],
    redirect_uri='http://localhost:8000/oauth2callback')

class AuthMixin(object):
    def dispatch(self, *args ,**kwargs):
        if self.request.user and self.request.user.is_authenticated():
            storage = Storage(CredentialsModel, 'id', self.request.user, 'credential')
            credential = storage.get()
            if credential is None or credential.invalid == True:
                FLOW.params['state'] = xsrfutil.generate_token(settings.SECRET_KEY,
                                                               self.request.user)
                authorize_url = FLOW.step1_get_authorize_url()
                return HttpResponseRedirect(authorize_url)
            return super(AuthMixin, self).dispatch(*args, **kwargs)
        else:
            return HttpResponseRedirect('/accounts/login/')

# @login_required
# def index(request):
#   storage = Storage(CredentialsModel, 'id', request.user, 'credential')
#   credential = storage.get()
#   if credential is None or credential.invalid == True:
#     FLOW.params['state'] = xsrfutil.generate_token(settings.SECRET_KEY,
#                                                    request.user)
#     authorize_url = FLOW.step1_get_authorize_url()
#     return HttpResponseRedirect(authorize_url)
#   else:
#     http = httplib2.Http()
#     http = credential.authorize(http)
#     service = build("calendar", "v3", http=http)
#     now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
#     eventsResult = service.events().list(
#         calendarId='primary', timeMin=now, maxResults=10, singleEvents=True,
#         orderBy='startTime').execute()
#     events = eventsResult.get('items', [])
#
#     # activities = service.activities()
#     # activitylist = activities.list(collection='public',
#     #                                userId='me').execute()
#     # logging.info(activitylist)
#
#     return render(request, 'welcome.html', {
#                 'events': events,
#                 })
class ListEvent(AuthMixin, TemplateView):
    template_name = 'events/list.html'

    def get_context_data(self, **kwargs):
        context = super(ListEvent, self).get_context_data(**kwargs)
        now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
        googleapi_helpclass = SingletoneGoogleApi(self.request.user)
        profiles = googleapi_helpclass.get_profiles()
        profile_id = googleapi_helpclass.get_first_profile_id()
        context['profiles'] = profiles
        return context


class RealTimeEvent(AuthMixin, TemplateView):
    template_name = 'events/realtime.html'

    def get_context_data(self, **kwargs):
        context = super(RealTimeEvent, self).get_context_data(**kwargs)
        googleapi_helpclass = SingletoneGoogleApi(self.request.user)
        now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
        profiles = googleapi_helpclass.get_profiles()
        profile_id, site = googleapi_helpclass.get_first_profile_id()
        metrics = googleapi_helpclass.get_metrics(str(profile_id))
        context['metrics'] = metrics
        context['site'] = site
        return context

@login_required
def auth_return(request):
  # if not xsrfutil.validate_token(settings.SECRET_KEY, request.GET['state'],
  #                                request.user.id):
  #   return  HttpResponseBadRequest()
  credential = FLOW.step2_exchange(request.GET)
  storage = Storage(CredentialsModel, 'id', request.user, 'credential')
  storage.put(credential)
  return HttpResponseRedirect("/welcome/")


class AddEvent(AuthMixin, FormView):
    template_name = 'events/add.html'
    form_class = AddSiteForm
    success_url = '/success/'
    def form_valid(self, form):
        googleapi_helpclass = SingletoneGoogleApi(self.request.user)
        web_site_name = form.cleaned_data['name']
        web_site_url = form.cleaned_data['url']
        googleapi_helpclass.insert_webProperty(web_site_url, web_site_name)
        return HttpResponseRedirect(self.success_url)



